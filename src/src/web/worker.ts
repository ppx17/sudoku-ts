import {OptimizedGenerator} from "lib/generator/optimizedGenerator";
import {Generator} from "lib/generator/generator";
import {Difficulty} from "lib/difficulty";
import {BoardSerializer} from "lib/board";

const generator: Generator = new OptimizedGenerator();

onmessage = (e) => {
    const difficulty = e.data as Difficulty;
    generator.generate(difficulty).then((board) => {
        postMessage(BoardSerializer.Serialize(board));
    });
}
import {SudokuGame} from "lib/sudokuGame";

function main() {
    const domElement = document.createElement('div');
    document.body.appendChild(domElement);

    const sudoku = new SudokuGame(domElement);

    sudoku.initialize();
}

window.onload = main;
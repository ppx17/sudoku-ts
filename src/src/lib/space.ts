import {EventHandler} from "./eventHandler";

export interface InputSpaceEventArgs {
    space: InputSpace;
}

export class Space {
    public readonly x: number;
    public readonly y: number;
    public readonly options: Options;
    protected value?: number = null;
    protected fixed: boolean = false;

    constructor(x: number, y: number, options: null | Options = new Options(true)) {
        this.x = x;
        this.y = y;
        this.options = options;
    }

    clone(): Space {
        const clone = new Space(this.x, this.y, this.options.clone());
        clone.value = this.value;
        clone.fixed = this.fixed;

        return clone;
    }

    markFixed(fixed: boolean = true): void {
        this.fixed = fixed;
    }

    hasValue() {
        return this.value !== null;
    }

    isFixed(): boolean {
        return this.fixed;
    }

    blocks(space: Space): boolean {
        if (space.x === this.x && space.y === this.y) return false;

        return space.x === this.x ||
            space.y === this.y ||
            (Math.floor(space.x / 3) === Math.floor(this.x / 3)
                && Math.floor(space.y / 3) === Math.floor(this.y / 3));
    }

    setValue(number?: number): null | number {
        if (this.isFixed()) {
            console.error(`Attempting to update ${this.x}:${this.y} which was marked as fixed`);
            return null;
        }

        number = Number(number);

        if (number >= 1 && number <= 9) {
            this.value = number;
            return number;
        }

        this.value = null;
        return null;
    }

    getValue(): number | null {
        return this.value;
    }

    clear() {
        this.value = null;
        this.fixed = false;
    }

    sees(other: Space): boolean {
        return this.x === other.x
            || this.y === other.y
            || (
                Math.floor(this.x / 3) === Math.floor(other.x / 3)
                && Math.floor(this.y / 3) === Math.floor(other.y / 3)
            );
    }

    location(): string {
        return `${['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'][this.y]}${this.x + 1}`;
    }
}

export class InputSpace extends Space {
    public element: Element;
    public readonly noteGrid: NoteGrid;
    private eventHandler: null | EventHandler = null;
    private focused: boolean = false;

    constructor(x: number, y: number, options: null | Options = new Options(false)) {
        super(x, y, options);
        this.noteGrid = new NoteGrid(this.options);
    }

    setEventHandler(eventHandler: EventHandler) {
        this.eventHandler = eventHandler;
        this.eventHandler.addEventListener('space-focused', (data: InputSpaceEventArgs) => {
            if (data.space === this) return;
            this.removeFocus();
        });
        this.eventHandler.addEventListener('space-updated', (data: InputSpaceEventArgs) => {
            if (!data.space.hasValue() || this.hasValue()) return;
            if (data.space.blocks(this)) {
                this.options.close(data.space.getValue());
            }
        });
        this.noteGrid.setEventHandler(eventHandler);
    }

    setElement(input: Element): void {
        this.element = input;
        this.element.addEventListener('click', this.onFocus.bind(this));
        this.setInputValue(this.value);
    }

    setValue(number?: number): null | number {
        const result = super.setValue(number);
        this.markInvalid(false);
        this.suggest(false);
        if (result !== null) {
            this.markInvalid(false);
        }
        this.setInputValue(result);
        return result;
    }

    markFixed(mark: boolean = true): void {
        super.markFixed(mark);
        this.setClass("fixed", mark);
    }

    markInvalid(mark: boolean = true): void {
        this.setClass("invalid", mark);
    }

    highlight(mark: boolean = true): void {
        this.setClass("highlight", mark);
    }

    highlightOptions(options: number[], color: HighlightColor): void {
        this.noteGrid.highlight(options, color);
    }

    resetHighlightOptions(): void {
        this.noteGrid.resetHighlight();
    }

    suggest(mark: boolean = true): void {
        this.setClass('suggested', mark);
    }

    focus(): void {
        this.focused = true;
        this.setClass("focus", true);
        this.dispatchFocusEvent();
    }

    hasFocus(): boolean {
        return this.focused;
    }

    hasInput(): boolean {
        return this.element !== undefined;
    }

    clear() {
        super.clear();
        this.markFixed(false);
        this.markInvalid(false);
        this.highlight(false);
        this.setInputValue(null);
        this.options.fromNumberList([]);
        this.noteGrid.resetHighlight();
    }

    dispatchSpaceUpdated() {
        if (this.eventHandler !== null) {
            this.eventHandler.dispatchEvent('space-updated', {space: this});
        }
    }

    private removeFocus(): void {
        this.focused = false;
        this.setClass("focus", false);
    }

    private setClass(cssClass: string, active: boolean): void {
        if (!this.hasInput()) return;
        if (active) {
            this.element.classList.add(cssClass);
        } else {
            this.element.classList.remove(cssClass);
        }
    }

    private onFocus(): void {
        this.focus();
    }

    private setInputValue(number?: number): void {
        if (this.hasInput()) {
            if (number === null) {
                this.element.innerHTML = '';
                this.element.appendChild(this.noteGrid.getElement());
            } else {
                this.element.textContent = number.toString();
            }
        }
    }

    private dispatchFocusEvent() {
        if (this.eventHandler !== null) {
            this.eventHandler.dispatchEvent('space-focused', {space: this});
        }
    }
}

export enum HighlightColor {
    Blue = 'highlight-blue',
    Green = 'highlight-green',
    Red = 'highlight-red',
}

export type NoteState = Map<number, boolean>;

export class Options {
    public readonly state: NoteState;
    public onchange: Function;

    constructor(initial: boolean = true) {
        this.state = new Map<number, boolean>([
            [1, initial], [2, initial], [3, initial],
            [4, initial], [5, initial], [6, initial],
            [7, initial], [8, initial], [9, initial],
        ]);
    }

    get optionList(): number[] {
        let options: number[] = [];
        this.state.forEach((value, key) => {
            if (value) {
                options.push(key);
            }
        });
        return options;
    }

    get hasSingleOption(): boolean {
        return this.optionCount === 1;
    }

    get optionCount(): number {
        return this.optionList.length;
    }

    get firstOption(): null | number {
        return this.optionList[0];
    }

    clone(): Options {
        const clone = new Options();
        this.state.forEach((value, key) => clone.set(key, value, false));
        clone.callOnChange();
        return clone;
    }

    openAllOptions() {
        this.state.forEach((value, key) => this.set(key, true, false));
        this.callOnChange();
    }

    public get(i: number): boolean {
        return this.state.has(i) && this.state.get(i);
    }

    public toggle(i: number): void {
        this.set(i, !this.get(i));
    }

    public open(i: number): void {
        this.set(i, true);
    }

    public close(i: number): void {
        this.set(i, false);
    }

    public set(i: number, state: boolean, callOnChange: boolean = true): void {
        if (this.state.has(i)) {
            this.state.set(i, state);
        }
        if (callOnChange) this.callOnChange();
    }

    public fromNumberList(numbers: number[]) {
        let changes = 0;
        this.state.forEach((current: boolean, i: number) => {
            const wanted: boolean = numbers.find(n => i === n) !== undefined;
            if (current !== wanted) {
                this.set(i, numbers.find(n => i === n) !== undefined, false);
                changes++;
            }
        });
        if (changes > 0) {
            this.callOnChange();
        }
    }

    public toNumberList(): number[] {
        let numbers: number[] = [];
        this.state.forEach((value, key) => {
            if (value) {
                numbers.push(key);
            }
        });
        return numbers;
    }

    public hasOpen(i: number) {
        return this.state.has(i) && this.state.get(i);
    }

    private callOnChange() {
        if (this.onchange !== undefined) {
            this.onchange();
        }
    }
}

class NoteGrid {
    private readonly options: Options;
    private readonly element: HTMLElement;
    private events: EventHandler | null = null;
    private highlights: Map<HighlightColor, number[]> = new Map();

    constructor(options: Options) {
        this.element = document.createElement('div');
        this.element.className = 'note-grid';
        this.options = options;
        this.options.onchange = this.optionsChanged.bind(this);
        this.update();
    }

    public setEventHandler(events: EventHandler) {
        this.events = events;
    }

    public getElement(): HTMLElement {
        return this.element;
    }

    public optionsChanged() {
        this.update();
    }

    public resetHighlight() {
        if (!this.hasHighlights()) return;

        this.highlights = new Map();
        this.update();
    }

    public highlight(options: number[], color: HighlightColor) {
        this.highlights.set(color, options);
        this.update();
    }

    private update() {
        this.element.innerHTML = '';
        this.options.state.forEach((isActive: boolean, number: number) => {
            const el = document.createElement('div');
            el.classList.add('note-number');
            if (isActive) {
                el.innerText = number.toString();
                this.highlights.forEach((options, color) => {
                    if (options.indexOf(number) !== -1) {
                        el.classList.add(color);
                    }
                });
            } else {
                el.innerHTML = '&nbsp;';
            }
            this.element.appendChild(el);
        });
        this.dispatchNoteUpdated();
    }

    private dispatchNoteUpdated() {
        if (this.events !== null) {
            this.events.dispatchEvent('note-updated');
        }
    }

    private hasHighlights(): boolean {
        for (const [color, numbers] of this.highlights) {
            if (numbers.length > 0) return true;
        }
        return false;
    }
}
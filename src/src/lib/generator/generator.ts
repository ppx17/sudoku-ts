import {Board} from "lib/board";
import {Solver, SolverFactory} from "lib/solver";
import {Space} from "lib/space";
import {Difficulty, DifficultyComparer, DifficultyRater, Rating} from "lib/difficulty";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";
import {DifficultyRaterFactory} from "lib/strategies";

export {PredefinedGenerator} from "./predefinedGenerator";
export {OptimizedGenerator} from "./optimizedGenerator";

abstract class BaseGenerator {
    protected readonly solver: Solver = SolverFactory.getSolver();

    protected clearRandomSpace(board: Board): boolean {
        const space = this.pickRandomFilledSpace(board);
        return this.attemptToClearSpace(space, board);
    }

    protected attemptToClearSpace(space: Space, board: Board) {
        const value = space.getValue();
        space.setValue(null);

        if (this.boardIsValid(board)) {
            return true;
        }
        space.setValue(value);
        return false;
    }

    protected boardIsValid(board: Board): boolean {
        return this.solver.hasSingleSolution(board);
    }

    protected pickRandomFilledSpace(board: Board): Space {
        return this.shuffle(this.shuffle(this.getFilledSpaces(board)))[0];
    }

    protected getFilledSpaces(board: Board): Space[] {
        return board.spaces.filter(s => s.hasValue());
    }

    protected shuffle<T>(input: T[]): T[] {
        return ArrayHelpers.shuffle(input);
    }

    protected randomFilledBoard(): Board {
        const board = new Board();
        board.generateSpaces();
        this.solver.solveRandom(board);
        return board;
    }

    protected generateBoard(): Board {
        const board = this.randomFilledBoard();

        let cleared = 0;
        for (let i = 0; i < 60; i++) {
            cleared += this.clearRandomSpace(board) ? 1 : 0;
        }
        return board;
    }
}

export interface Generator {
    generate(difficulty: Difficulty): Promise<Board>;
}


export class LinearGenerator extends BaseGenerator implements Generator {
    private readonly rater: DifficultyRater = DifficultyRaterFactory.create();
    private readonly comparer = new DifficultyComparer();

    async generate(difficulty: Difficulty): Promise<Board> {
        return new Promise((resolve) => {
            resolve(this.generateDifficulty(difficulty));
        })
    }

    public generateMaxDifficulty(targetDifficulty: Difficulty): Rating {

        let currentDifficulty = Difficulty.Easy;
        let currentRating: Rating;
        const board = this.randomFilledBoard();

        for (let i = 0; i < 120; i++) {

            const space = this.pickRandomFilledSpace(board);
            const value = space.getValue();
            space.setValue(null);

            let newRating = this.rater.rate(board);

            if (newRating.difficulty === Difficulty.Invalid) {
                space.setValue(value);
                continue;
            }

            if (newRating.difficulty === Difficulty.Unknown) {
                // The rater doesn't have expert strategies yet, but if it cannot solve a valid puzzle, then we
                // can assume it's expert level.
                newRating.difficulty = Difficulty.Expert;
            }

            if (!this.comparer.lowerOrEqual(newRating.difficulty, targetDifficulty)) {
                // board became to difficult, reset space and try again
                space.setValue(value);
                continue;
            }

            currentDifficulty = newRating.difficulty;
            currentRating = newRating;
        }

        return currentRating;
    }

    private generateDifficulty(targetDifficulty: Difficulty): Board {

        while (true) {
            const boardWithRating = this.generateMaxDifficulty(targetDifficulty);

            if (!this.comparer.lower(boardWithRating.difficulty, targetDifficulty)) {
                return boardWithRating.board;
            }

            console.log('Board was not difficult enough, restarting');
        }
    }
}

export class RandomGenerator extends BaseGenerator implements Generator {

    async generate(difficulty: Difficulty): Promise<Board> {
        return new Promise((resolve) => {
            this.status(`Generating new ${difficulty} game`);
            switch (difficulty) {
                case Difficulty.Easy:
                    resolve(this.generateEasyBoard());
                    return;
                case Difficulty.Medium:
                    resolve(this.generateMediumBoard());
                    return;
                case Difficulty.Hard:
                    resolve(this.generateHardBoard());
                    return;
            }
        });
    }

    private generateEasyBoard(): Board {
        const board = this.generateBoard();

        if (this.solver.solveEasy(board.clone())) {
            return board;
        } else {
            this.status("Had to throw board away");
            return this.generateEasyBoard();
        }
    }

    private generateMediumBoard(): Board {
        const board = this.generateBoard();

        if (!this.solver.solveEasy(board.clone()) && this.solver.solveMedium(board.clone())) {
            return board;
        } else {
            console.warn("Had to throw board away");
            return this.generateMediumBoard();
        }
    }

    private generateHardBoard(): Board {
        const board = this.generateBoard();

        if (!this.solver.solveMedium(board.clone())) {
            let cleared = this.attemptToClearAllSpaces(board);
            while (cleared > 0) {
                cleared = this.attemptToClearAllSpaces(board);
            }
            return board;
        } else {
            return this.generateHardBoard();
        }
    }

    private attemptToClearAllSpaces(board: Board): number {
        let result = 0;
        this.getFilledSpaces(board).forEach(s => {
            result += this.attemptToClearSpace(s, board) ? 1 : 0;
        });
        return result;
    }

    private status(message: String): void {
        console.log(message);
    }
}
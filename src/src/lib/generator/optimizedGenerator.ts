import {Difficulty} from "lib/difficulty";
import {Generator, LinearGenerator, PredefinedGenerator} from "lib/generator/generator";
import {Board} from "lib/board";

export class OptimizedGenerator implements Generator {

    private delegates: Map<Difficulty, Generator> = new Map();

    constructor() {
        const linear = new LinearGenerator();
        const predefined = new PredefinedGenerator();

        this.delegates.set(Difficulty.Easy, linear);
        this.delegates.set(Difficulty.Medium, linear);
        this.delegates.set(Difficulty.Advanced, predefined);
        this.delegates.set(Difficulty.Hard, predefined);
        this.delegates.set(Difficulty.Expert, linear);
    }

    generate(difficulty: Difficulty): Promise<Board> {
        return this.delegates.get(difficulty).generate(difficulty);
    }
}
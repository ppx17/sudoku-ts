import {Generator} from "lib/generator/generator";
import {Board, BoardSerializer, StoredBoard} from "lib/board";
import {Difficulty, DifficultySerializer} from "lib/difficulty";
import {Randomizer} from "lib/randomizer";

export class PredefinedGenerator implements Generator {
    private puzzleCache: Map<Difficulty, PredefinedPuzzle[]> = new Map();
    private randomizer: Randomizer = new Randomizer();

    generate(difficulty: Difficulty): Promise<Board> {
        return new Promise(async (resolve) =>  {
            if(!this.puzzleCache.has(difficulty)) {
                const puzzles = await this.retrieve(difficulty);
                this.puzzleCache.set(difficulty, puzzles);
            }
            const puzzle = this.pickRandomPuzzle(this.puzzleCache.get(difficulty));
            const board = BoardSerializer.BoardFromData(puzzle.puzzle);
            resolve(this.randomizeBoard(board));
        });
    }

    retrieve(difficulty: Difficulty): Promise<PredefinedPuzzle[]> {
        const url = `puzzles/${DifficultySerializer.serialize(difficulty).toLowerCase()}.ndjson`;

        return new Promise((resolve, err) => {
            fetch(url)
                .then((response) => {
                    response
                        .text()
                        .then(text => {
                            resolve(text.split("\n")
                                .filter(line => line !== "")
                                .map(line => JSON.parse(line)));

                        });
                })
                .catch(exception => err(exception));

        })
    }

    private pickRandomPuzzle(puzzles: PredefinedPuzzle[]): PredefinedPuzzle {
        return puzzles[Math.floor(Math.random() * puzzles.length)];
    }

    private randomizeBoard(board: Board): Board {
        return this.randomizer.randomize(board);
    }
}

interface PredefinedPuzzle {
    filled: number;
    rating: string;
    steps: string[];
    puzzle: StoredBoard;
}
import {Generator} from "lib/generator/generator";
import {Board, BoardSerializer} from "lib/board";
import {Difficulty} from "lib/difficulty";

export class WebWorkerGenerator implements Generator {

    private worker = new Worker('worker.js');
    private callback: null | ((board: Board) => void) = null;
    private workerBusy: boolean = false;

    constructor() {
        this.worker.onmessage = ev => {
            if (this.callback !== null) {
                const board = BoardSerializer.Deserialize(ev.data);
                this.callback(board);
                this.callback = null;
                this.workerBusy = false;
            }
        }
    }

    generate(difficulty: Difficulty): Promise<Board> {
        if (this.workerBusy) {
            console.log('Worker is already busy, ignoring request');
            return Promise.reject("Worker is busy, please try again later");
        }
        this.worker.postMessage(difficulty);
        this.workerBusy = true;
        return new Promise((resolve) => {
            this.callback = resolve;
        })
    }
}
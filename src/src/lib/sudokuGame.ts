import {Renderer} from "./renderer";
import {EventHandler} from "./eventHandler";
import {ControlMode, Controls, ModeToggledEventArgs, NumberControls} from "./controls";
import {HighlightColor, InputSpace, NoteState} from "./space";
import {Board, RenderedBoard} from "./board";
import {Validator} from "./validator";
import {Solver, SolverFactory} from "./solver";
import {Generator} from "./generator/generator";
import {DelayedExecutor, GameStorage} from "./storage";
import {OptionReducer} from "./algorithms/optionReducer";
import {NeighborReducer} from "./algorithms/reducers/neighborReducer";
import {ExposedPairReducer} from "./algorithms/reducers/exposedPairReducer";
import {HiddenPairReducer, HiddenQuadReducer, HiddenTripletReducer} from "./algorithms/reducers/hiddenSetReducers";
import {PointingPairReducer} from "./algorithms/reducers/pointingPairReducer";
import {XwingReducer} from "./algorithms/reducers/xwingReducer";
import {SquareLineReducer} from "./algorithms/reducers/squareLineReducer";
import {SingleChainReducer} from "./algorithms/reducers/singleChainReducer";
import {HiddenSingleReducer} from "./algorithms/reducers/hiddenSingleReducer";
import {YwingReducer} from "./algorithms/reducers/ywingReducer";
import {ReduceSuggestion} from "./algorithms/reducers/reducer";
import {XYZwingReducer} from "./algorithms/reducers/xyzwingReducer";
import {XyChainsReducer} from "./algorithms/reducers/xyChainsReducer";
import {Difficulty, DifficultyRater} from "./difficulty";
import {AlgorithmSuggesterFactory, DifficultyRaterFactory} from "./strategies";
import {Randomizer} from "lib/randomizer";
import {WebWorkerGenerator} from "lib/generator/webWorkerGenerator";

export class SudokuGame {
    private domElement: Element;
    private gameElement: HTMLDivElement;
    private readonly generator: Generator = new WebWorkerGenerator();
    private readonly events: EventHandler = new EventHandler();
    private readonly solver: Solver = SolverFactory.getSolver();
    private readonly validator: Validator = new Validator();
    private readonly gameStorage: GameStorage = new GameStorage(window.localStorage, this.events);
    private readonly controls: Controls = new Controls(this.events, this.gameStorage.settings);
    private readonly rater: DifficultyRater = DifficultyRaterFactory.create();
    private readonly suggester = AlgorithmSuggesterFactory.create();
    private board: RenderedBoard;
    private statusLabel: HTMLDivElement;
    private numberControls: NumberControls;
    private activeSuggestion: ReduceSuggestion | null = null;
    private readonly removeHighlightDelay: DelayedExecutor = new DelayedExecutor(50);

    constructor(domElement: Element) {
        this.domElement = domElement;
        this.insertContainerDivs();
        this.attachEvents();
        this.initializeRenderedBoard();
    }

    public initialize() {
        this.events.dispatchEvent('initialize');
    }

    public newBoard() {
        this.gameElement.classList.add("generating");
        this.updateStatus('Searching for a new puzzle, this could take a while...');

        this.generator.generate(this.gameStorage.settings.difficulty)
            .then((board: Board) => {
                this.board.clear();
                this.board.copyBoardState(board);
                this.board.spaces[0].focus();
                this.board.spaces.forEach(s => s.hasValue() && s.markFixed());

                this.rater.rateAsync(this.board).then((rating) => {
                    if (rating.difficulty === Difficulty.Unknown) {
                        this.updateStatus(`Puzzle is rated as Expert. I don't know all strategies needed to solve this puzzle.`);
                    } else {
                        this.updateStatus(`Puzzle is rated as ${rating.difficulty} (${rating.score}). ` +
                            `The hardest required strategy is ${rating.hardestStrategy?.reducer?.name}`);
                    }
                });
                this.gameElement.classList.remove("generating");
            })
            .catch(() => {
                console.log('Received no board from generator');
            });
    }

    private initializeRenderedBoard() {
        this.board = new RenderedBoard();
        this.board.generateSpaces();
        this.board.setEventHandler(this.events);
        this.gameElement.innerHTML = '';
        new Renderer(this.gameElement, this.board).render();
    }

    private insertContainerDivs() {
        this.gameElement = document.createElement('div');
        this.gameElement.className = "game";
        this.domElement.appendChild(this.gameElement);

        this.statusLabel = document.createElement('div');
        this.statusLabel.className = "status";
        this.domElement.appendChild(this.statusLabel);

        const numberButtonContainer = document.createElement('div');
        numberButtonContainer.className = "number-controls";
        this.numberControls = new NumberControls(this.events);
        this.numberControls.appendElements(numberButtonContainer);
        this.domElement.appendChild(numberButtonContainer);

        const controlsContainer = document.createElement('div');
        this.controls.appendElements(controlsContainer);
        this.domElement.appendChild(controlsContainer);
    }

    private attachEvents() {
        this.attachButtonEvents();
        this.attachGameEvents();
        this.attachWindowEvents();
    }

    private attachButtonEvents() {
        this.events.addMultipleListeners({
            'btn-new-game-clicked': () => {
                if (this.confirm('start a new game')) this.newBoard();
            },
            'btn-solve-clicked': () => {
                if (!this.confirm('solve this puzzle')) {
                    return;
                }
                const solvable = this.solver.solveIncreasing(this.board);
                this.events.dispatchEvent(solvable ? 'board-solved' : 'board-unsolvable');
            },
            'btn-validate-clicked': () => {
                this.updateStatus(this.validator.validate(this.board) && this.board.isSolvable()
                    ? (this.board.isFinished()
                        ? "You've solved the puzzle correctly!"
                        : "You're still on track!")
                    : "You've made an error");
            },
            'btn-reset-game-clicked': () => {
                if (this.confirm('remove all your input')) this.board.clearFilled();
            },
            'btn-highlight-mistakes-clicked': () => {
                this.updateStatus(this.mistakesMessage(
                    this.board.forEachError((s: InputSpace) => s.markInvalid()),
                    'highlight',
                    'highlighted'
                ));
            },
            'btn-remove-mistakes-clicked': () => {
                this.updateStatus(this.mistakesMessage(
                    this.board.forEachError((s: InputSpace) => s.setValue(null)),
                    'remove',
                    'removed'
                ));
            },
            'btn-clear-clicked': () => {
                if (!this.confirm('clear all numbers from the grid')) {
                    return;
                }
                this.board.clear();
            },
            'btn-notes-clicked': () => this.events.dispatchEvent('toggle-mode'),
            'btn-help-clicked': () => this.events.dispatchEvent('help-requested'),
            'btn-fill-notes-clicked': () => {
                if (this.confirmNotes('replace all notes with generated notes')) {
                    this.events.dispatchEvent('fill-notes');
                }
            }
        });
    }

    private mistakesMessage(mistakes: null | number, actionFuture: string, actionPast: string): string {
        switch (mistakes) {
            case null:
                return `Could not find a solution for the current puzzle.`;
            case 0:
                return `You're still on track, nothing to ${actionFuture}!`;
            case 1:
                return `There was a single mistake, it is ${actionPast}.`;
            default:
                return `There were ${mistakes} mistakes and they're ${actionPast}.`;
        }
    }

    private attachGameEvents() {
        this.events.addMultipleListeners({
            'initialize': () => {
                if (this.gameStorage.hasSavedGame()) {
                    this.events.dispatchEvent('load-game');
                } else {
                    this.newBoard()
                }
            },
            'status-update': (data: StatusUpdate) => {
                this.statusLabel.innerText = data.message;
            },
            'board-solved': () => {
                this.updateStatus("Solved!");
                this.gameElement.classList.add("finished");
                setTimeout(() => this.gameElement.classList.remove("finished"), 6000);
            },
            'board-unsolvable': () => {
                this.updateStatus("😖 This current board seems to be unsolvable.");
            },
            'clear-save': () => {
                if (this.gameStorage.hasSavedGame()) {
                    if (confirm(`Are you sure you want to delete your safe game?`)) {
                        this.gameStorage.clear();
                        this.updateStatus('Your save game has been cleared!');
                    }
                }
            },
            'save-game': () => {
                this.gameStorage.save(this.board);
            },
            'load-game': () => {
                if (!this.gameStorage.hasSavedGame()) {
                    this.updateStatus('You had no saved game.');
                    return;
                }

                this.updateStatus('Loading save game...');

                const loadedBoard = this.gameStorage.load();
                this.board.clear();
                this.board.copyBoardState(loadedBoard, true);

                this.updateStatus('Your saved game is loaded.');
            },
            'space-updated': () => {
                // Auto saving
                this.events.dispatchEvent('save-game');

                this.removeHighlightDelay.execute(this.removeSuggestionsIfApplied.bind(this));

                if (this.board.isFinished()) {
                    if (this.validator.validate(this.board)) {
                        this.events.dispatchEvent('board-solved');
                    } else {
                        this.events.dispatchEvent('btn-highlight-mistakes-clicked');
                    }
                }
            },
            'note-updated': () => {
                this.events.dispatchEvent('save-game');
                this.removeHighlightDelay.execute(this.removeSuggestionsIfApplied.bind(this));
            },
            'toggle-mode': () => {
                this.numberControls.mode = (this.numberControls.mode === ControlMode.Number)
                    ? ControlMode.Note
                    : ControlMode.Number;
                this.events.dispatchEvent('mode-toggled', {mode: this.numberControls.mode});
            },
            'mode-toggled': (data: ModeToggledEventArgs) => {
                if (data.mode === ControlMode.Note) {
                    this.gameElement.classList.add('note-mode');
                } else {
                    this.gameElement.classList.remove('note-mode');
                }
            },
            'focus-up': () => this.moveFocus(0, -1),
            'focus-down': () => this.moveFocus(0, 1),
            'focus-left': () => this.moveFocus(-1, 0),
            'focus-right': () => this.moveFocus(1, 0),
            'fill-notes': () => {
                this.board.spaces.forEach((space: InputSpace) => {
                    if (space.hasValue()) return;
                    let options: NoteState = new Map();
                    for (let i = 1; i <= 9; i++) {
                        options.set(i, true);
                    }
                    this.board.row(space.y)
                        .concat(this.board.col(space.x))
                        .concat(this.board.square(space.x, space.y))
                        .forEach((neighbor: InputSpace) => {
                            if (!neighbor.hasValue()) return;
                            options.set(neighbor.getValue(), false);
                        });
                    let valid: number[] = [];
                    options.forEach((selected: boolean, index: number) => {
                        if (selected) {
                            valid.push(index);
                        }
                    });
                    space.options.fromNumberList(valid);
                })
            },
            'promote-single': () => {
                this.board.spaces.forEach((space: InputSpace) => {
                    if (space.hasValue()) return;
                    if (space.options.hasSingleOption) {
                        space.setValue(space.options.firstOption);
                        space.dispatchSpaceUpdated();
                    }
                });
            },
            'auto-solve': () => {
                const solver = new OptionReducer([
                    new NeighborReducer(),
                    new HiddenSingleReducer(),
                    new ExposedPairReducer(),
                    new HiddenPairReducer(),
                    new HiddenTripletReducer(),
                    new HiddenQuadReducer(),
                    new PointingPairReducer(),
                    new SquareLineReducer(),
                    new XwingReducer(),
                    new YwingReducer(),
                    new XYZwingReducer(),
                    new SingleChainReducer(),
                    new XyChainsReducer(),
                ]);
                const board = this.board.clone();
                solver.progress(board);
                this.board.playOptionsFromBoard(board);
            },
            'settings-updated': () => this.gameStorage.saveSettings(),
            'help-requested': () => {
                const suggestion = this.suggester.suggest(this.board);

                this.removeSuggestionsIfApplied();

                this.updateStatus(suggestion.message);

                if (suggestion.reduceSuggestion !== null) {
                    suggestion.reduceSuggestion.effects.forEach(e => {
                        e.space instanceof InputSpace && e.space.highlightOptions(e.removeOptions, HighlightColor.Red);
                    });
                    suggestion.reduceSuggestion.causes.forEach(e => {
                        if (!(e.space instanceof InputSpace)) return;
                        if (e.highlightSpace) e.space.suggest(true);
                        if (e.highlightBlue.length) e.space.highlightOptions(e.highlightBlue, HighlightColor.Blue);
                        if (e.highlightGreen.length) e.space.highlightOptions(e.highlightGreen, HighlightColor.Green);
                    });
                    this.activeSuggestion = suggestion.reduceSuggestion;
                } else {
                    suggestion.suggestedSpaces.forEach(s => s instanceof InputSpace && s.suggest(true));
                    this.activeSuggestion = null;
                }
            },
            'randomize-board': () => {
                const randomizer = new Randomizer();
                const random = randomizer.randomize(this.board);
                this.board.clear();
                this.board.copyBoardState(random, true);
            }

        });
    }

    private removeSuggestionsIfApplied() {
        if (this.activeSuggestion === null || this.activeSuggestion.isApplied()) {
            this.board.spaces.forEach(s => {
                s.suggest(false);
                s.resetHighlightOptions();
            });
        }
    }

    private attachWindowEvents() {
        const keyToEventMap: { [s: string]: string } = {
            'ArrowUp': 'focus-up',
            'ArrowDown': 'focus-down',
            'ArrowLeft': 'focus-left',
            'ArrowRight': 'focus-right',
            'n': 'btn-new-game-clicked',
            'v': 'btn-validate-clicked',
            's': 'btn-solve-clicked',
            'r': 'btn-remove-mistakes-clicked',
            'h': 'btn-help-clicked',
            'c': 'btn-clear-clicked',
            ' ': 'toggle-mode',
            'o': 'fill-notes',
            'p': 'promote-single',
            'a': 'auto-solve',
            'b': 'randomize-board',
        };

        window.addEventListener('keydown', (e) => {
            if (e.key.match(/^\d$/)) {
                this.events.dispatchEvent('number-key-pressed', {number: Number(e.key)});
                return;
            }
            if (e.key === "Backspace" || e.key === "Delete") {
                this.events.dispatchEvent('number-key-pressed', {number: 0});
                return;
            }

            const event = keyToEventMap[e.key];
            if (event === undefined) return;
            e.preventDefault();
            this.events.dispatchEvent(event);
        });
    }

    private moveFocus(x: number, y: number) {
        const focussed = this.board.focusedSpace();
        if (focussed === undefined) {
            return;
        }

        x = (x + focussed.x + this.board.width) % this.board.width;
        y = (y + focussed.y + this.board.height) % this.board.height;

        const newSpace = this.board.board[y][x];
        if (newSpace === undefined || !(newSpace instanceof InputSpace) || !newSpace.hasInput()) return;
        newSpace.focus();
    }

    private updateStatus(status: string) {
        this.events.dispatchEvent('status-update', {message: status});
    }

    private confirm(action: string): boolean {
        if (this.board === null || !this.board.isInProgress() || this.board.isFinished()) {
            return true;
        }

        return window.confirm(`Are you sure you want to ${action}? You seem to have a game in progress and you will lose that progress.`);
    }

    private confirmNotes(action: string): boolean {
        if (this.board === null || this.board.isFinished()) {
            return true;
        }
        if (this.board.spaces.find(s => s.options.optionCount > 0) === undefined) {
            return true;
        }
        return window.confirm(`Are you sure you want to ${action}? You currently have notes entered and will lose those.`);
    }
}

interface StatusUpdate {
    message: string;
}
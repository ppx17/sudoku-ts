export class EventHandler {
    private eventListeners: Map<string, Array<Function>>;

    constructor() {
        this.eventListeners = new Map<string, Array<Function>>();
    }

    addEventListener(event: string, handler: Function): void {
        if (!this.eventListeners.has(event)) {
            this.eventListeners.set(event, []);
        }

        this.eventListeners.get(event).push(handler);
    }

    dispatchEvent(event: string, data: object = {}) {
        if (!this.eventListeners.has(event)) {
            return;
        }

        this.eventListeners.get(event).forEach((handler: Function) => handler(data));
    }

    addMultipleListeners(bindings: { [s: string]: Function }) {
        for (const [event, handler] of Object.entries(bindings)) {
            this.addEventListener(event, handler)
        }
    }
}
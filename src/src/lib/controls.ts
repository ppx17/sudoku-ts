import {EventHandler} from "./eventHandler";
import {InputSpace, InputSpaceEventArgs, NoteState} from "./space";
import {Settings} from "./settings";
import {Difficulty, DifficultySerializer} from "./difficulty";

export enum ControlMode {
    Number,
    Note
}

export interface ModeToggledEventArgs {
    mode: ControlMode;
}

export class NumberControls {
    private events: EventHandler;
    private inputs: Map<number, HTMLElement> = new Map();
    private activeSpace: InputSpace | null = null;

    constructor(events: EventHandler) {
        this.events = events;
        this.events.addEventListener('space-focused', (event: InputSpaceEventArgs) => {
            this.activeSpace = event.space;
            this.updateForActiveSpace();
        });
        this.events.addEventListener('space-updated', () => {
            this.updateForActiveSpace();
        });
        this.events.addEventListener('number-key-pressed', (data: { [number: string]: number }) => {
            if (this.activeSpace === null) return;
            if (this.mode === ControlMode.Number) {
                this.activeSpace.setValue(this.activeSpace.getValue() === data.number ? null : data.number);
                this.activeSpace.dispatchSpaceUpdated();
            } else {
                if (!this.activeSpace.hasValue()) {
                    this.activeSpace.options.toggle(data.number);
                    this.updateForActiveSpace();
                }
            }
        });
    }

    private _mode: ControlMode = ControlMode.Number;

    get mode(): ControlMode {
        return this._mode;
    }

    set mode(mode: ControlMode) {
        this._mode = mode;
        this.updateForActiveSpace();
    }

    appendElements(container: Element) {
        for (let i = 1; i <= 9; i++) {
            const input = document.createElement('span');
            input.innerText = i.toString();
            input.className = "number-input";
            input.onclick = () => {
                this.events.dispatchEvent('number-key-pressed', {number: i});
            }
            container.appendChild(input);
            this.inputs.set(i, input);
        }
    }

    private updateForActiveSpace() {
        if (this.activeSpace === null) return;
        if (this.mode === ControlMode.Number) {
            this.highlight(this.activeSpace.hasValue() ? this.activeSpace.getValue() : 0);
        } else {
            this.highlightNoteState(this.activeSpace.options.state);
        }
    }

    private highlight(number: number) {
        this.inputs.forEach((el, elementNumber) => {
            this.highlightElement(el, number === elementNumber);
        })
    }

    private highlightNoteState(noteState: NoteState) {
        this.inputs.forEach((el, elementNumber) => {
            this.highlightElement(el, noteState.get(elementNumber) === true);
        });
    }

    private highlightElement(element: HTMLElement, highlight: boolean) {
        if (highlight) {
            element.classList.add('highlight');
        } else {
            element.classList.remove('highlight');
        }
    }
}

export class Controls {
    private list: Element;
    private events: EventHandler;
    private settings: Settings;

    constructor(events: EventHandler, settings: Settings) {
        this.events = events;
        this.settings = settings;
    }

    appendElements(container: Element) {
        this.list = document.createElement('ul');
        this.list.className = "controls";
        this.createButton('✏️ Notes', 'btn-notes-clicked');
        this.createButton('ℹ️ <u>H</u>elp', 'btn-help-clicked');
        this.createButton('✨️ Fill n<u>o</u>tes', 'btn-fill-notes-clicked');
        this.createButton('✅ <u>V</u>alidate', 'btn-validate-clicked');
        this.createButton('<u>S</u>olve', 'btn-solve-clicked');
        this.createButton('<u>N</u>ew Game', 'btn-new-game-clicked');
        this.createButton('Reset Game', 'btn-reset-game-clicked');
        this.createButton('Highlight Mistakes', 'btn-highlight-mistakes-clicked');
        this.createButton('<u>R</u>emove Mistakes', 'btn-remove-mistakes-clicked');
        this.createButton('<u>C</u>lear', 'btn-clear-clicked');
        this.createDifficultySelector();

        container.appendChild(this.list);
    }

    private createButton(label: string, eventName: string): void {
        const input = document.createElement('button');
        input.innerHTML = label;
        input.onclick = () => this.events.dispatchEvent(eventName, {element: input});

        this.appendToList(input);
    }

    private createDifficultySelector() {
        const hidden = ['Unknown', 'Invalid'];
        const input = document.createElement('select');
        for (let difficulty of Object.keys(Difficulty)) {
            if (hidden.indexOf(difficulty) !== -1) continue;
            const option = document.createElement('option');
            option.text = difficulty;
            option.value = difficulty;
            input.options.add(option);
        }
        input.onchange = (e) => {
            if (e.target instanceof HTMLSelectElement) {
                const selectedDifficulty = DifficultySerializer.deserialize(e.target.value);
                if (selectedDifficulty !== undefined) {
                    this.settings.difficulty = selectedDifficulty;
                }
            }
        };
        input.selectedIndex = Object.values(Difficulty).indexOf(this.settings.difficulty);
        this.appendToList(input);
    }

    private appendToList(element: HTMLElement) {
        const li = document.createElement('li');
        li.appendChild(element);
        this.list.appendChild(li);
    }
}
import {Board} from "./board";
import {Algorithm} from "./algorithms/algorithms";
import {BacktrackingMultipleSolutionFinder, IncBacktracking, RndBacktracking} from "./algorithms/backtracking";
import {OptionReducer} from "./algorithms/optionReducer";
import {NeighborReducer} from "./algorithms/reducers/neighborReducer";
import {ExposedPairReducer} from "./algorithms/reducers/exposedPairReducer";
import {HiddenPairReducer} from "./algorithms/reducers/hiddenSetReducers";
import {PointingPairReducer} from "./algorithms/reducers/pointingPairReducer";
import {HiddenSingleReducer} from "./algorithms/reducers/hiddenSingleReducer";

export class SolverFactory {
    private static solver: Solver;

    static getSolver(): Solver {
        SolverFactory.solver ??= new Solver();
        return SolverFactory.solver;
    }
}

export class Solver {
    private readonly cache: Cache = new SolutionCache();
    private readonly multipleSolutionFinder: BacktrackingMultipleSolutionFinder = new BacktrackingMultipleSolutionFinder();

    public hasSingleSolution(board: Board): boolean {
        return this.multipleSolutionFinder.numberOfSolutions(board) === 1;
    }

    solveIncreasing(board: Board): boolean {
        return this.solve(board, new IncBacktracking(), this.cache);
    }

    solveRandom(board: Board): boolean {
        return this.solve(board, new RndBacktracking(), new NullCache());
    }

    solveEasy(board: Board): boolean {
        return this.solve(board, new OptionReducer([
            new NeighborReducer(),
            new HiddenSingleReducer(),
        ]), new NullCache());
    }

    solveMedium(board: Board): boolean {
        return this.solve(board, new OptionReducer([
            new NeighborReducer(),
            new HiddenSingleReducer(),
            new ExposedPairReducer(),
            new HiddenPairReducer(),
            new PointingPairReducer(),
        ]), new NullCache());
    }

    private solve(board: Board, algorithm: Algorithm, cache: Cache): boolean {
        if (board.allSpacesFilled()) {
            return true;
        }

        const cachedSolution = cache.pull(board);
        if (cachedSolution !== null) {
            board.copyBoardState(cachedSolution, false);
            return cachedSolution.allSpacesFilled();
        }

        algorithm.progress(board);

        return board.allSpacesFilled();
    }
}

interface Cache {
    put(solution: Board): void;

    pull(partial: Board): null | Board;
}

class NullCache implements Cache {
    pull(partial: Board): Board | null {
        return null;
    }

    put(solution: Board): void {
        // noop
    }
}

class SolutionCache implements Cache {
    private solutions: Map<string, Board> = new Map<string, Board>();

    put(solution: Board): void {
        const serial = solution.serialize();
        if (this.solutions.has(serial)) {
            return;
        }
        if (serial.match(/0/)) {
            return;
        }
        this.solutions.set(serial, solution.clone());
    }

    pull(partial: Board): null | Board {
        const partialSerial = partial.serialize();
        if (partialSerial.match(/^0+$/)) {
            return null;
        }

        for (let cachedSerial of this.solutions.keys()) {
            if (this.compareSerials(partialSerial, cachedSerial)) {
                return this.solutions.get(cachedSerial);
            }
        }
        return null;
    }

    private compareSerials(a: string, b: string): boolean {
        for (let [idx, char] of a.split('').entries()) {
            if (char === "0" || b[idx] === "0") {
                continue;
            }
            if (char !== b[idx]) {
                return false;
            }
        }
        return true;
    }
}
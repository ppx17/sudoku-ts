import {EventHandler} from "./eventHandler";
import {Difficulty} from "./difficulty";

export class Settings implements SettingsObject {
    private events: EventHandler;

    constructor(events: EventHandler) {
        this.events = events;
    }

    private _difficulty: Difficulty = Difficulty.Easy;

    get difficulty(): Difficulty {
        return this._difficulty;
    }

    set difficulty(difficulty: Difficulty) {
        this._difficulty = difficulty;
        this.events.dispatchEvent('settings-updated', this);
    }

    public adopt(source: SettingsObject) {
        this._difficulty = source.difficulty;
    }

    public serialize(): SettingsObject {
        return {
            difficulty: this._difficulty
        };
    }
}

export interface SettingsObject {
    difficulty: Difficulty;
}
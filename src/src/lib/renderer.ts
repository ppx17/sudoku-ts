import {Board} from "./board";
import {InputSpace, Space} from "./space";

export class Renderer {
    private readonly container: HTMLElement;
    private readonly board: Board;

    private readonly rowLabels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
    private readonly labelClass = 'labels';
    private readonly tableClass = 'sudoku';

    constructor(container: HTMLElement, board: Board) {
        this.container = container;
        this.board = board;
    }

    render() {
        const table = document.createElement('table');
        table.className = this.tableClass;

        const body = table.createTBody();
        this.renderColumnNames(body);
        this.board.board.forEach((row: Space[], index) => {
            this.renderRow(body, index, row);
        });
        this.container.appendChild(table);
    }

    private renderRow(body: HTMLTableSectionElement, rowNumber: number, spaces: Space[]) {
        const tr = body.insertRow();
        tr.insertCell().innerText = this.rowLabels[rowNumber];
        spaces.forEach(space => {
            const td = tr.insertCell();
            if (space instanceof InputSpace) {
                space.setElement(td);
            }
        });
    }

    private renderColumnNames(body: HTMLTableSectionElement) {
        const row = body.insertRow();
        row.className = this.labelClass;
        row.insertCell();
        for (let i = 1; i <= 9; i++) {
            row.insertCell().innerText = String(i);
        }
    }
}
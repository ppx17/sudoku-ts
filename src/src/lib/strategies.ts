import {NeighborReducer} from "./algorithms/reducers/neighborReducer";
import {HiddenSingleReducer} from "./algorithms/reducers/hiddenSingleReducer";
import {ExposedPairReducer} from "./algorithms/reducers/exposedPairReducer";
import {Difficulty, DifficultyRater} from "./difficulty";
import {HiddenPairReducer, HiddenQuadReducer, HiddenTripletReducer} from "./algorithms/reducers/hiddenSetReducers";
import {PointingPairReducer} from "./algorithms/reducers/pointingPairReducer";
import {SquareLineReducer} from "./algorithms/reducers/squareLineReducer";
import {XwingReducer} from "./algorithms/reducers/xwingReducer";
import {YwingReducer} from "./algorithms/reducers/ywingReducer";
import {XYZwingReducer} from "./algorithms/reducers/xyzwingReducer";
import {SingleChainReducer} from "./algorithms/reducers/singleChainReducer";
import {XyChainsReducer} from "./algorithms/reducers/xyChainsReducer";
import {AlgorithmSuggester} from "./algorithmSuggester";
import {SingleOptionFiller} from "./algorithms/fillers/singleOptionFiller";
import {Reducer} from "./algorithms/reducers/reducer";
import {OptionReducer} from "./algorithms/optionReducer";

export class RatedReducer {
    constructor(public readonly reducer: Reducer, public readonly score: number) {
    }
}

export class DifficultyRaterFactory {
    public static create(): DifficultyRater {
        return new DifficultyRater(ReducerRepository.ratedReducers, [
            {difficulty: Difficulty.Easy, maxHardestDifficulty: 2, maxTotalScore: 15},
            {difficulty: Difficulty.Medium, maxHardestDifficulty: 6, maxTotalScore: 50},
            {difficulty: Difficulty.Advanced, maxHardestDifficulty: 10, maxTotalScore: 100},
            {difficulty: Difficulty.Hard, maxHardestDifficulty: 15, maxTotalScore: 200},
        ])
    }
}

export class AlgorithmSuggesterFactory {
    public static create(): AlgorithmSuggester {
        return new AlgorithmSuggester([
                new SingleOptionFiller(),
            ],
            ReducerRepository.reducers
        );
    }
}

export class OptionReducerFactory {
    public static create(): OptionReducer {
        return new OptionReducer(ReducerRepository.reducers)
    }
}

class ReducerRepository {
    public static readonly ratedReducers: RatedReducer[] = [
        new RatedReducer(new NeighborReducer(), 1),
        new RatedReducer(new HiddenSingleReducer(), 1),
        new RatedReducer(new ExposedPairReducer(), 2),
        new RatedReducer(new HiddenPairReducer(), 4),
        new RatedReducer(new PointingPairReducer(), 4),
        new RatedReducer(new SquareLineReducer(), 6),
        new RatedReducer(new HiddenTripletReducer(), 6),
        new RatedReducer(new XwingReducer(), 8),
        new RatedReducer(new YwingReducer(), 10),
        new RatedReducer(new HiddenQuadReducer(), 10),
        new RatedReducer(new SingleChainReducer(), 10),
        new RatedReducer(new XYZwingReducer(), 12),
        new RatedReducer(new XyChainsReducer(), 14),
    ];

    public static get reducers(): Reducer[] {
        return this.ratedReducers.map((rr: RatedReducer) => rr.reducer);
    }
}
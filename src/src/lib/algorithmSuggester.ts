import {Reducer, ReduceSuggestion} from "./algorithms/reducers/reducer";
import {Board} from "./board";
import {Filler} from "./algorithms/fillers/filler";
import {Space} from "./space";
import {NeighborReducer} from "./algorithms/reducers/neighborReducer";

export class AlgorithmSuggester {
    private reducers: Reducer[];
    private fillers: Filler[];

    constructor(filler: Filler[], reducers: Reducer[]) {
        this.fillers = filler;
        this.reducers = reducers;
    }

    suggest(original: Board): Suggestion {

        const single = this.suggestSingleOption(original);
        if (single !== null) return single;

        const noNotes = this.suggestNoNotes(original);
        if (noNotes !== null) return noNotes;

        const reducer = this.suggestReducer(original);
        if (reducer !== null) return reducer;

        return Suggestion.make(`I'm sorry, I cannot find any more suggestions for this puzzle. I clearly still have lots to learn. 🧐`);
    }

    private spacesWithoutOptions(board: Board): Space[] {
        return board.spaces.filter(s => !s.hasValue() && s.options.toNumberList().length === 0);
    }

    private suggestSingleOption(original: Board): null | Suggestion {
        const board = original.clone();
        board.spaces.forEach(s => s.options.openAllOptions());

        const nr = new NeighborReducer();
        nr.reduceOptions(board);

        const filler = this.fillers.find(f => f.fillOptions(board));
        if (filler !== undefined) {
            return Suggestion.make(`There are some spots with just 1 option, we can fill those already.`)
                .suggestSpaces(this.findFilledSpaces(board, original));
        }
        return null;
    }

    private suggestNoNotes(original: Board): null | Suggestion {
        const spacesWithoutOptions = this.spacesWithoutOptions(original);
        if (spacesWithoutOptions.length > 0) {
            return Suggestion.make('You have empty spaces without options filled. ' +
                'All other algorithms are option reducers, who require you to have filled the options. ' +
                'You can hit the "Fill notes" button below to automatically fill all available options.');
        }
        return null;
    }

    private suggestReducer(original: Board): null | Suggestion {
        const reduceSuggestion = this.findReduceSuggestion(original);

        if (reduceSuggestion !== null) {
            const message = reduceSuggestion.help === undefined
                ? `We can reduce some options using the '${reduceSuggestion.reducer.name.toLowerCase()}' strategy.`
                : reduceSuggestion.help;
            return Suggestion.make(
                message
            )
                .suggestReduceSuggestion(reduceSuggestion);
        }

        return null;
    }

    private findReduceSuggestion(original: Board): null | ReduceSuggestion {
        for (const reducer of this.reducers) {
            const suggestion = reducer.suggest(original);
            if (suggestion !== null) {
                return suggestion;
            }
        }

        return null;
    }

    private findFilledSpaces(board: Board, original: Board): Space[] {
        return original
            .spaces
            .filter(s => !s.hasValue())
            .filter(s => board.board[s.y][s.x].hasValue());
    }

    private findReducedSpaces(board: Board, original: Board): Space[] {
        return original
            .spaces
            .filter(s => !s.hasValue())
            .filter(s => board.board[s.y][s.x].options.optionCount !== s.options.optionCount);
    }
}

export class Suggestion {
    public suggestedSpaces: Space[] = [];
    public reduceSuggestion: null | ReduceSuggestion = null;

    constructor(public message: string) {
    }

    static make(message: string) {
        return new Suggestion(message);
    }

    suggestSpaces(spaces: Space[]): Suggestion {
        this.suggestedSpaces = spaces;
        return this;
    }

    suggestReduceSuggestion(suggestion: ReduceSuggestion): Suggestion {
        this.reduceSuggestion = suggestion;
        return this;
    }
}
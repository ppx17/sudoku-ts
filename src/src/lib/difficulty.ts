import {Board} from "./board";
import {Solver} from "./solver";
import {NeighborReducer} from "./algorithms/reducers/neighborReducer";
import {SingleOptionFiller} from "./algorithms/fillers/singleOptionFiller";
import {Filler} from "./algorithms/fillers/filler";
import {RatedReducer} from "./strategies";

export class DifficultyRater {
    private readonly solver: Solver = new Solver();
    private readonly maxIterations: number = 81 * 9;
    private readonly neighborReducer: NeighborReducer = new NeighborReducer();
    private readonly filler: Filler = new SingleOptionFiller()

    constructor(private reducers: RatedReducer[], private rules: DifficultyRule[]) {
    }

    public rateAsync(original: Board): Promise<Rating> {
        return new Promise((resolve) => {
            resolve(this.rate(original));
        })
    }

    public rate(original: Board): Rating {
        const rating = new Rating(original);

        if (this.hasMultipleSolutions(original)) {
            rating.difficulty = Difficulty.Invalid;
            return rating;
        }

        const board = original.clone();
        const emptySpaces = board.spaces.filter(s => !s.hasValue()).length;

        rating.difficulty = Difficulty.Easy;

        if (emptySpaces < 9) {
            rating.difficulty = Difficulty.Easy;
            return rating;
        }

        board.spaces.forEach(s => s.options.openAllOptions());
        while (this.neighborReducer.reduceOptions(board)) {
        }
        this.filler.fillOptions(board);


        if (board.isFinished()) {
            rating.difficulty = Difficulty.Easy;
            return rating;
        }

        for (let i = 0; i < this.maxIterations; i++) {
            let couldReduce = false;
            let couldFill = this.filler.fillOptions(board);

            for (const reduction of this.reducers) {
                const suggestion = reduction.reducer.suggest(board);
                if (suggestion === null) continue;

                rating.addReduction(reduction);
                suggestion.applyEffects();
                couldReduce = true;
                break;
            }

            if (!couldReduce && !couldFill) break;
        }

        if (!board.allSpacesFilled()) {
            rating.difficulty = Difficulty.Unknown;
        } else {
            rating.difficulty = this.determineDifficulty(rating);
        }

        return rating;
    }

    private hasMultipleSolutions(original: Board): boolean {
        return !this.solver.hasSingleSolution(original.clone());
    }

    private determineDifficulty(rating: Rating): Difficulty {

        for (const rule of this.rules) {
            if (rating.score <= rule.maxTotalScore
                && rating.hardestStrategy
                && rating.hardestStrategy.score <= rule.maxHardestDifficulty) {
                return rule.difficulty;
            }
        }

        return Difficulty.Expert;
    }
}

export class Rating {
    public reductions: RatedReducer[] = [];
    public difficulty: Difficulty = Difficulty.Unknown;

    public constructor(public board: Board) {
    }

    public get score(): number {
        return this.reductions.map(r => r.score).reduce((x, y) => x + y, 0);
    }

    public get hardestStrategy(): undefined | RatedReducer {
        const hardestScore = this.reductions.map(r => r.score).reduce((x, y) => x > y ? x : y, 0);
        return this.reductions.find(rr => rr.score === hardestScore);
    }

    public addReduction(reducer: RatedReducer) {
        this.reductions.push(reducer);
    }
}

export interface DifficultyRule {
    maxTotalScore: number;
    maxHardestDifficulty: number;
    difficulty: Difficulty;
}

export enum Difficulty {
    Easy = 'Easy',
    Medium = 'Medium',
    Advanced = 'Advanced',
    Hard = 'Hard',
    Expert = 'Expert',
    Unknown = 'Unknown',
    Invalid = 'Invalid'
}

export class DifficultySerializer {
    public static serialize(difficulty: Difficulty): string {
        return String(difficulty);
    }

    public static deserialize(str: string, def: Difficulty | undefined = undefined): undefined | Difficulty {
        str = `${str.charAt(0).toUpperCase()}${str.substr(1).toLowerCase()}`;
        const mayBeDifficulty: Difficulty | undefined = (<any>Difficulty)[str];
        return mayBeDifficulty === undefined ? def : mayBeDifficulty;
    }
}

export class DifficultyComparer {
    private map = new Map<Difficulty, number>();

    constructor() {
        this.map.set(Difficulty.Easy, 1);
        this.map.set(Difficulty.Medium, 2);
        this.map.set(Difficulty.Advanced, 3);
        this.map.set(Difficulty.Hard, 4);
        this.map.set(Difficulty.Expert, 5);
    }

    public incomparable(d: Difficulty): boolean {
        return !this.map.has(d);
    }

    public lowerOrEqual(a: Difficulty, b: Difficulty): boolean {
        if (this.incomparable(a) || this.incomparable(b)) return false;
        return this.map.get(a) <= this.map.get(b);
    }

    public lower(a: Difficulty, b: Difficulty): boolean {
        if (this.incomparable(a) || this.incomparable(b)) return false;
        return this.map.get(a) < this.map.get(b);
    }
}

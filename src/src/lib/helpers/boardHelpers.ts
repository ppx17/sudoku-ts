import {Board} from "lib/board";
import {Space} from "lib/space";

export enum BoardRange {
    Row,
    Column,
    Square
}

export class BoardHelpers {
    public static findDynamicRange<T>(board: Board, range: BoardRange, callable: (board: Board, spaces: Space[]) => null | T): null | T {
        switch (range) {
            case BoardRange.Column:
                return BoardHelpers.findColumn<T>(board, callable);
            case BoardRange.Row:
                return BoardHelpers.findRow<T>(board, callable);
            case BoardRange.Square:
                return BoardHelpers.findSquare<T>(board, callable);
        }
        return null;
    }

    public static forDynamicRange(board: Board, range: BoardRange, callable: (board: Board, spaces: Space[]) => void) {
        switch (range) {
            case BoardRange.Column:
                BoardHelpers.forEachColumn(board, callable);
                break;
            case BoardRange.Row:
                BoardHelpers.forEachRow(board, callable);
                break;
            case BoardRange.Square:
                BoardHelpers.forEachSquare(board, callable);
                break;
        }
    }

    public static findSquare<T>(board: Board, callable: (board: Board, spaces: Space[]) => null | T): null | T {
        for (let x = 0; x < 9; x += 3) {
            for (let y = 0; y < 9; y += 3) {
                let result = callable(board, board.square(x, y));
                if (result !== null) {
                    return result;
                }
            }
        }
        return null;
    }

    public static forEachSquare(board: Board, callable: (board: Board, spaces: Space[]) => void) {
        for (let x = 0; x < 9; x += 3) {
            for (let y = 0; y < 9; y += 3) {
                callable(board, board.square(x, y));
            }
        }
    }

    public static findRow<T>(board: Board, callable: (board: Board, spaces: Space[]) => null | T): null | T {
        for (let i = 0; i < 9; i++) {
            let result = callable(board, board.row(i));
            if (result !== null) {
                return result;
            }
        }
        return null;
    }

    public static forEachRow(board: Board, callable: (board: Board, spaces: Space[]) => void) {
        for (let i = 0; i < 9; i++) {
            callable(board, board.row(i));
        }
    }

    public static findColumn<T>(board: Board, callable: (board: Board, spaces: Space[]) => null | T): null | T {
        for (let i = 0; i < 9; i++) {
            let result = callable(board, board.col(i));
            if (result !== null) {
                return result;
            }
        }
        return null;
    }

    public static forEachColumn(board: Board, callable: (board: Board, spaces: Space[]) => void) {
        for (let i = 0; i < 9; i++) {
            callable(board, board.col(i));
        }
    }

    public static findRange<T>(board: Board, callable: (board: Board, spaces: Space[]) => null | T): null | T {
        let result = null;
        result ??= BoardHelpers.findRow(board, callable);
        result ??= BoardHelpers.findColumn(board, callable);
        result ??= BoardHelpers.findSquare(board, callable);
        return result;
    }

    public static forEachRange(board: Board, callable: (board: Board, spaces: Space[]) => void) {
        BoardHelpers.forEachRow(board, callable);
        BoardHelpers.forEachColumn(board, callable);
        BoardHelpers.forEachSquare(board, callable);
    }

    public static forEachLine(board: Board, callable: (board: Board, spaces: Space[]) => void) {
        BoardHelpers.forEachRow(board, callable);
        BoardHelpers.forEachColumn(board, callable);
    }

    public static numberToSpacesMap(spaces: Space[]): Map<number, Space[]> {
        const numberToSpaces: Map<number, Space[]> = new Map();

        for (let i = 1; i <= 9; i++) {
            numberToSpaces.set(i, []);
        }

        spaces.forEach((s: Space) => {
            if (s.hasValue()) return;
            for (let n of s.options.toNumberList()) {
                numberToSpaces.get(n).push(s);
            }
        });

        return numberToSpaces;
    }
}
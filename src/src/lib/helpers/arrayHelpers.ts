export class ArrayHelpers {

    public static crossJoin<T>(list: T[], callback: (a: T, b: T) => void): void {
        if (list.length < 2) return;
        const set = list.filter(_ => true); // re-index
        for (let ai = 0; ai < list.length - 1; ai++) {
            for (let bi = ai + 1; bi < list.length; bi++) {
                callback(set[ai], set[bi]);
            }
        }
    }

    public static crossJoinTriplet<T>(list: T[], callback: (a: T, b: T, c: T) => void): void {
        if (list.length < 3) return;
        const set = list.filter(_ => true); // re-index
        for (let ai = 0; ai < list.length - 2; ai++) {
            for (let bi = ai + 1; bi < list.length - 1; bi++) {
                for (let ci = bi + 1; ci < list.length; ci++) {
                    callback(set[ai], set[bi], set[ci]);
                }
            }
        }
    }

    public static crossJoinQuad<T>(list: T[], callback: (a: T, b: T, c: T, d: T) => void): void {
        if (list.length < 4) return;
        const set = list.filter(_ => true); // re-index
        for (let ai = 0; ai < list.length - 3; ai++) {
            for (let bi = ai + 1; bi < list.length - 2; bi++) {
                for (let ci = bi + 1; ci < list.length - 1; ci++) {
                    for (let di = ci + 1; di < list.length; di++) {
                        callback(set[ai], set[bi], set[ci], set[di]);
                    }
                }
            }
        }
    }

    public static crossJoinSets<T>(setSize: number, list: T[], callback: (...set: T[]) => void): void {
        switch (setSize) {
            case 2:
                this.crossJoin(list, callback);
                return;
            case 3:
                this.crossJoinTriplet(list, callback);
                return;
            case 4:
                this.crossJoinQuad(list, callback);
                return;
        }
        throw new Error(`Only sets of 2, 3 and 4 are supported, got ${setSize}`);
    }

    public static arraysAreEqual<T>(a: T[], b: T[]): boolean {
        if (a === b) return true;
        if (a.length !== b.length) return false;
        for (let i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }

    public static intersect<T>(a: T[], b: T[]): T[] {
        return a.filter(x => b.includes(x));
    }

    public static shuffle<T>(input: T[]): T[] {
        return input.sort(() => Math.random() - 0.5);
    }
}
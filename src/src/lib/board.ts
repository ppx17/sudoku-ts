import {InputSpace, InputSpaceEventArgs, Options, Space} from "./space";
import {Solver, SolverFactory} from "./solver";
import {EventHandler} from "./eventHandler";

export class Board {
    public board: Space[][] = [];
    public spaces: Space[] = [];
    public readonly width: number = 9;
    public readonly height: number = 9;
    protected readonly solver: Solver = SolverFactory.getSolver();
    protected eventHandler?: EventHandler = null;
    private readonly squareCache: Map<number, Space[]> = new Map();

    public static squareIndices(i: number): number[] {
        return [
            [0, 1, 2],
            [0, 1, 2],
            [0, 1, 2],
            [3, 4, 5],
            [3, 4, 5],
            [3, 4, 5],
            [6, 7, 8],
            [6, 7, 8],
            [6, 7, 8]
        ][i];
    }

    clone(): Board {
        const clonedBoard = new Board();
        this.spaces.forEach(space => {
            let clonedSpace = space.clone();
            clonedBoard.board[space.y] ??= [];
            clonedBoard.board[space.y][space.x] = clonedSpace;
            clonedBoard.spaces.push(clonedSpace);
        });

        return clonedBoard;
    }

    solution(): null | Board {
        const solution = this.clone();
        solution.clearFilled();
        return this.solver.solveIncreasing(solution) ? solution : null;
    }

    isSolvable(): boolean {
        return this.solver.solveIncreasing(this.clone());
    }

    isFinished(): boolean {
        return this.allSpacesFilled() && this.isSolvable();
    }

    allSpacesFilled(): boolean {
        return this.spaces.filter(s => !s.hasValue()).length === 0;
    }

    serialize(): string {
        return this.spaces.map(s => s.hasValue() ? s.getValue() : 0).join('');
    }

    firstRow(callable: (spaces: Space[]) => boolean): null | Space[] {
        for (let y = 0; y < this.width; y++) {
            let row = this.row(y);
            if (callable(row)) {
                return row;
            }
        }
        return null;
    }

    firstColumn(callable: (spaces: Space[]) => boolean): null | Space[] {
        for (let x = 0; x < this.width; x++) {
            let column = this.col(x);
            if (callable(column)) {
                return column;
            }
        }
        return null;
    }

    firstSquare(callable: (spaces: Space[]) => boolean): null | Space[] {
        for (let y = 0; y < this.height; y += 3) {
            for (let x = 0; x < this.width; x += 3) {
                let square = this.square(x, y);
                if (callable(square)) {
                    return square;
                }
            }
        }
        return null;
    }

    row(y: number): Space[] {
        return this.board[y];
    }

    col(x: number): Space[] {
        return this.board.map(r => r[x]);
    }

    square(x: number, y: number): Space[] {
        const si = [0, 0, 0, 1, 1, 1, 2, 2, 2];
        const index = si[x] + (si[y] * 3);
        if (!this.squareCache.has(index)) {

            let result = [];
            for (let iy of Board.squareIndices(y)) {
                for (let ix of Board.squareIndices(x)) {
                    result.push(this.board[iy][ix]);
                }
            }
            this.squareCache.set(index, result);
        }

        return this.squareCache.get(index);
    }

    space(label: string): null | Space {
        label = label.toLowerCase();
        if (!label.match(/^[a-i][1-9]$/)) return null;
        const row = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'].indexOf(label.charAt(0));
        const col = Number(label.charAt(1)) - 1;

        return this.board[row][col];
    }

    sees(target: Space): Space[] {
        const spaces = new Set<Space>();
        this.row(target.y).forEach(s => spaces.add(s));
        this.col(target.x).forEach(s => spaces.add(s));
        this.square(target.x, target.y).forEach(s => spaces.add(s));
        spaces.delete(target);

        return [...spaces];
    }

    clearFilled() {
        this.spaces.forEach(space => {
            if (space.isFixed() || !space.hasValue()) {
                return;
            }
            space.setValue(null);
        });
    }

    clear() {
        this.spaces.forEach(s => s.clear());
    }

    isInProgress() {
        return this.spaces.find(s => !s.isFixed() && s.hasValue()) !== undefined;
    }

    copyBoardState(source: Board, includeFixed: boolean = false) {
        this.spaces.forEach(s => {
            const sourceSpace = source.board[s.y][s.x];
            if (includeFixed) {
                s.clear();
                s.setValue(sourceSpace.getValue());
                s.markFixed(sourceSpace.isFixed());
                s.options.fromNumberList(sourceSpace.options.toNumberList());
                return;
            }

            if (s.isFixed()) {
                return;
            }

            if (sourceSpace.getValue() !== 0) {
                s.setValue(sourceSpace.getValue());
            }
        });
    }

    playOptionsFromBoard(board: Board) {
        this.spaces.forEach((space: InputSpace) => {
            if (space.hasValue()) return;
            if (board.board[space.y][space.x].hasValue()) {
                space.setValue(board.board[space.y][space.x].getValue());
            } else {
                space.options.fromNumberList(board.board[space.y][space.x].options.toNumberList());
            }
        });
    }

    generateSpaces(openAllOptions: boolean = true) {
        this.spaces = [];
        this.board = [];
        for (let y = 0; y < this.height; y++) {
            this.board.push([]);
            for (let x = 0; x < this.width; x++) {
                const space = this.makeSpace(x, y, new Options(openAllOptions));
                this.board[y].push(space);
                this.spaces.push(space);
            }
        }
    }

    isLegit(space: Space, number: number): boolean {
        return this.col(space.x).find(s => s.getValue() === number) === undefined
            && this.row(space.y).find(s => s.getValue() === number) === undefined
            && this.square(space.x, space.y).find(s => s.getValue() === number) === undefined;
    }

    // Allows subclasses to choose their own Space implementation
    protected makeSpace(x: number, y: number, options: null | Options = new Options(true)): Space {
        return new Space(x, y, options);
    }
}

export class RenderedBoard extends Board {
    public spaces: InputSpace[] = [];

    setEventHandler(eventHandler: EventHandler) {
        this.eventHandler = eventHandler;
        this.spaces.forEach(s => s.setEventHandler(this.eventHandler));
        this.eventHandler.addEventListener('space-focused', (data: InputSpaceEventArgs) => {
            this.highlightValue(data.space.getValue());
        });
    }

    forEachError(callback: (space: InputSpace, expected: number) => void): null | number {
        const solution = this.solution();
        if (solution === null) {
            return null;
        }

        let errors = 0;

        this.spaces
            .filter(s => !s.isFixed() && s.hasValue())
            .forEach(space => {
                const expected = solution.board[space.y][space.x].getValue();
                if (space.getValue() !== expected) {
                    callback(space, expected);
                    errors++;
                }
            });

        return errors;
    }

    focusedSpace(): InputSpace {
        return this.spaces.find(s => s.hasFocus());
    }

    highlightValue(value?: number): void {
        this.spaces.forEach(s => s.highlight(s.getValue() === value && value !== null));
    }

    protected makeSpace(x: number, y: number, options: null | Options = new Options(true)): Space {
        return new InputSpace(x, y, options);
    }
}

export interface StoredBoard {
    values: string;
    fixed: string;
    notes?: string[];
}

export class BoardSerializer {
    public static Serialize(board: Board, options: SerializationOptions = {}): string {
        const data: StoredBoard = {
            values: board.serialize(),
            fixed: board.spaces.map(s => s.isFixed() ? '1' : '0').join(''),
        };
        if (options.notes === undefined || options.notes === true) {
            data.notes = board.spaces.map(s => s.options.toNumberList().join(''));
        }
        return JSON.stringify(data);
    }

    public static Deserialize(json: string): null | Board {
        const data = JSON.parse(json);

        return this.BoardFromData(data);
    }

    public static BoardFromData(data: any): null | Board {
        if (data === null || data.values == null
            || data.values.length !== 81) {
            return null;
        }

        const board = new Board();
        for (let y = 0; y < 9; y++) {
            for (let x = 0; x < 9; x++) {
                const idx = y * 9 + x;
                const space = new Space(x, y);
                space.setValue(Number(data.values[idx]));
                if (data.fixed != null && data.fixed.length === 81) {
                    space.markFixed(data.fixed[idx] === '1');
                }
                if (data.notes !== undefined) {
                    space.options.fromNumberList(data.notes[idx].split('').map((x: string) => Number(x)));
                }
                board.board[y] ??= [];
                board.board[y][x] = space;
                board.spaces.push(space);
            }
        }

        return board;
    }
}

export interface SerializationOptions {
    notes?: boolean
}
import {Board} from "lib/board";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";
import {Space} from "lib/space";

export class Randomizer {
    public randomize(original: Board): Board {
        let board = original.clone();

        this.shuffleNumbers(board);

        this.applyMaybe(board, (b: Board) => this.apply(b, this.flipHorizontal));
        this.applyMaybe(board, (b: Board) => this.apply(b, this.flipVertical));

        this.apply(board, this.shuffleRows);

        board = this.reindex(board);

        return board;
    }

    private applyMaybe(board: Board, callable: (board: Board) => void): void {
        if(Math.random() > 0.5) callable(board);
    }

    private apply(board: Board, callable: (spaces: Space[][]) => Space[][]): void {
        board.board = callable(board.board);
    }

    private flipVertical(board: Space[][]): Space[][] {
        return board.reverse();
    }

    private flipHorizontal(board: Space[][]) {
        return board.map(row => row.reverse());
    }

    private shuffleRows(board: Space[][]): Space[][] {
        const slices = [
            board.slice(0, 3),
            board.slice(3, 6),
            board.slice(6, 9)
        ];

        const shuffled = slices.map(slice => ArrayHelpers.shuffle(slice));
        ArrayHelpers.shuffle(shuffled);

        return [...shuffled[0], ...shuffled[1], ...shuffled[2]];
    }

    private reindex(board: Board): Board {
        const indexed = new Board();

        for (let y = 0; y < 9; y++) {
            indexed.board[y] = [];
            for (let x = 0; x < 9; x++) {
                const space = new Space(x, y);
                space.setValue(board.board[y][x].getValue())
                space.markFixed(board.board[y][x].isFixed())
                indexed.spaces.push(space);
                indexed.board[y] ??= []
                indexed.board[y][x] = space;
            }
        }

        return indexed;
    }

    private shuffleNumbers(board: Board) {
        const source: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const dest = ArrayHelpers.shuffle(ArrayHelpers.shuffle(source));

        board.spaces.forEach(s => {
            if (!s.hasValue()) return;
            const wasFixed = s.isFixed();
            s.markFixed(false);
            s.setValue(dest[s.getValue() - 1]);
            s.markFixed(wasFixed);
        });
    }
}
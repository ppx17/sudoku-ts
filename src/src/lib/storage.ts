import {Board, BoardSerializer} from "./board";
import {Settings} from "./settings";
import {EventHandler} from "./eventHandler";

export class GameStorage {
    private readonly storage: Storage;
    private readonly saveGameKey = 'game';
    private readonly settingsKey = 'settings';
    private readonly events: EventHandler;
    private readonly saveDelayer = new DelayedExecutor();

    constructor(storage: Storage, events: EventHandler) {
        this.storage = storage;
        this.events = events;
    }

    private _settings: Settings = null;

    get settings(): Settings {
        if (this._settings === null) {
            this._settings = new Settings(this.events);
            let settingsJson = this.storage.getItem(this.settingsKey);
            if (settingsJson !== null) {
                this._settings.adopt(JSON.parse(settingsJson));
            }
        }
        return this._settings;
    }

    hasSavedGame(): boolean {
        return this.getSaveData() !== null;
    }

    load(): null | Board {
        const data = this.getSaveData();
        if (data === null) return null;

        return BoardSerializer.Deserialize(data);
    }

    save(board: Board) {
        this.saveDelayer.execute(() => this.storage.setItem(this.saveGameKey, BoardSerializer.Serialize(board)));
    }

    clear() {
        this.storage.removeItem(this.saveGameKey);
    }

    saveSettings() {
        this.storage.setItem(this.settingsKey, JSON.stringify(this._settings.serialize()));
        console.log('settings saved');
    }

    private getSaveData(): null | string {
        return this.storage.getItem(this.saveGameKey);
    }
}

export class DelayedExecutor {
    private readonly delayMs: number;
    private closure: Function;
    private handle: any = null;

    constructor(delayMs: number = 100) {
        this.delayMs = delayMs;
    }

    execute(closure: Function) {
        this.closure = closure;
        if (this.handle !== null) {
            clearTimeout(this.handle);
        }
        this.handle = setTimeout(() => {
            closure();
        }, this.delayMs);
    }
}
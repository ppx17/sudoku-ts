import {Board} from "./board";
import {Space} from "./space";

export class Validator {
    private board: Board;

    validate(board: Board): boolean {
        this.board = board;
        return this.validateRows()
            && this.validateColumns()
            && this.validateSquares();
    }

    private validateRows(): boolean {
        return this.board.firstRow((spaces) => !this.isValidSequence(spaces)) === null;
    }

    private validateColumns(): boolean {
        return this.board.firstColumn((spaces) => !this.isValidSequence(spaces)) === null;
    }

    private validateSquares(): boolean {
        return this.board.firstSquare((spaces) => !this.isValidSequence(spaces)) === null;
    }

    private isValidSequence(spaces: Space[]): boolean {
        const values = spaces.map(s => s.getValue()).filter(n => n !== null);
        return new Set(values).size === values.length;
    }
}
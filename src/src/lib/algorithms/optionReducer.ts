import {Algorithm} from "./algorithms";
import {Board} from "../board";
import {Reducer} from "./reducers/reducer";
import {SingleOptionFiller} from "./fillers/singleOptionFiller";
import {Filler} from "./fillers/filler";

export class OptionReducer implements Algorithm {
    private reducers: Reducer[];

    private fillers: Filler[] = [
        new SingleOptionFiller(),
    ]

    constructor(reducers: Reducer[]) {
        this.reducers = reducers;
    }

    progress(board: Board): boolean {
        board.spaces.forEach(s => s.options.openAllOptions());

        for (let i = 0; i < 81 * 9; i++) {
            const couldReduce = this.applyReducers(board);
            const couldFill = this.applyFillers(board);

            if (!couldReduce && !couldFill) {
                return board.allSpacesFilled();
            }
        }
        console.warn('OptionReducer reached limit of 81 iterations');
        return false;
    }

    applyReducers(board: Board): boolean {
        return this.reducers.find(reducer => reducer.reduceOptions(board)) !== undefined;
    }

    applyFillers(board: Board): boolean {
        this.fillers.forEach(f => f.resetFilledSomething());
        this.fillers.map(filler => filler.fillOptions(board));

        return this.fillers.find(f => f.hasFilledSomething()) !== undefined;
    }
}
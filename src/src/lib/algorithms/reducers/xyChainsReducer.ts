import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Route, XyChainPathValidator, XyChainsFactory} from "../chains/xy";
import {Color, XyChain} from "../chains/structs";
import {Space} from "lib/space";

export class XyChainsReducer extends BaseReducer implements Reducer {
    name: string = 'XY-Chains';
    chainFactory: XyChainsFactory = new XyChainsFactory();

    suggest(board: Board): ReduceSuggestion | null {
        const chains = this.chainFactory.analyseBoard(board);

        if (chains.length === 0) return null;

        for (let chain of chains) {

            const suggestion = this.optionsLookingAtBothEnds(board, chain);

            if (suggestion !== null) {
                return suggestion;
            }
        }

        return null;
    }

    private optionsLookingAtBothEnds(board: Board, chain: XyChain): null | ReduceSuggestion {

        const suggestion = new ReduceSuggestion(this);

        const pathValidator = new XyChainPathValidator(chain);

        for (let s of board.spaces) {
            if (
                s.hasValue()
                || !chain.colorMap
                || chain.colorMap.has(s) // space is part of chain
            ) continue;

            for (let option of s.options.toNumberList()) {

                const seesSpaces: Set<Space> = new Set();

                chain.colorMap.forEach((noteColors, space) => {
                    const color = noteColors.get(option);
                    if (color === Color.Uncolored) return;
                    if (s.sees(space) && s.options.hasOpen(option)) {
                        seesSpaces.add(space);
                    }
                });

                if (seesSpaces.size < 2) {
                    continue;
                }

                const spacesArray = Array.from(seesSpaces);
                for (const start of seesSpaces) {
                    const destinations = spacesArray.filter(s => s !== start);
                    const route = pathValidator.validPath(option, start, destinations);

                    if (route !== null) {
                        this.suggestClose(s, option, suggestion);
                        if (suggestion.hasEffect) {
                            this.highlightRoute(suggestion, chain, route);
                            suggestion.help = this.helpForRoute(route, option);
                            return suggestion;
                        }
                    }
                }
            }
        }

        return null;
    }

    private helpForRoute(route: Route, number: number): string {
        const locA = route.spaces()[0].location();
        const locB = route.lastSpace().location();

        return `This is a XY-chain, following these cells ${route.spaces().map(s => s.location()).join(', ')}. ` +
            `Both ends have a ${number}, one is colored green, the other blue. If you pick ${number} for ` +
            `${locA} then the red cell can't have ${number}. If you pick the other candidate for ${locA} then you can follow ` +
            `the chain until you force ${number} in ${locB}, in that case the red ${number} can also not exists, so you ` +
            `can eliminate it.`;
    }

    private highlightRoute(suggestion: ReduceSuggestion, chain: XyChain, route: Route): ReduceSuggestion {

        route.spaces().forEach(space => {
            const cause = suggestion.causeForSpace(space);

            chain.colorMap.get(space).colors.forEach((color, number) => {
                if (color === Color.Blue) {
                    cause.highlightBlue.push(number);
                } else if (color === Color.Green) {
                    cause.highlightGreen.push(number);
                }
            });
        });

        return suggestion;
    }

    private highlightChain(suggestion: ReduceSuggestion, chain: XyChain): ReduceSuggestion {

        chain.colorMap.forEach((colors, space) => {
            const cause = suggestion.causeForSpace(space);

            colors.colors.forEach((color, number) => {
                if (color === Color.Blue) {
                    cause.highlightBlue.push(number);
                } else if (color === Color.Green) {
                    cause.highlightGreen.push(number);
                }
            });
        });

        return suggestion;
    }
}
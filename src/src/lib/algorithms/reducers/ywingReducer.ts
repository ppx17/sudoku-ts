import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";

export class YwingReducer extends BaseReducer implements Reducer {
    name: string = 'Y-Wing';

    suggest(board: Board): ReduceSuggestion | null {
        for (let space of board.spaces) {
            const suggestion = this.checkSpace(board, space);
            if (suggestion !== null) {
                return suggestion;
            }
        }
        return null
    }

    private checkSpace(board: Board, keySpace: Space): null | ReduceSuggestion {
        if (keySpace.hasValue()) return null;
        if (keySpace.options.optionCount !== 2) return null;

        const seen = board.sees(keySpace);
        const options = keySpace.options.toNumberList();

        const validOptions: PotentialCorner[] = [];

        seen.filter(corner => corner.options.optionCount === 2)
            .filter((corner: Space) => {
                const cornerOptions = corner.options.toNumberList();
                const intersect = ArrayHelpers.intersect(options, cornerOptions);
                if (intersect.length === 1) {
                    validOptions.push({
                        space: corner,
                        intersect: intersect[0],
                        optionList: cornerOptions,
                    });
                }
            });

        if (validOptions.length < 2) {
            return null;
        }

        let foundSuggestion: null | ReduceSuggestion = null;
        ArrayHelpers.crossJoin<PotentialCorner>(validOptions, (a, b) => {
            if (foundSuggestion !== null) return;
            if (a.intersect === b.intersect) return;
            const common = ArrayHelpers.intersect(a.optionList, b.optionList);
            if (common.length !== 1) return;

            const suggestion = new ReduceSuggestion(this);
            // Now find spaces that are seen by both A and B, and we can remove the common from there
            const seenByCorners = ArrayHelpers.intersect(board.sees(a.space), board.sees(b.space));
            // Remove our key space
            seenByCorners.filter(s => s !== keySpace).forEach(s => this.suggestClose(s, common[0], suggestion));

            if (suggestion.hasEffect) {
                const options = keySpace.options.toNumberList();
                suggestion.causeForSpace(keySpace).highlightSpace = true;
                [keySpace, a.space, b.space].forEach(s => {
                    suggestion.causeForSpace(s).highlightBlue.push(...options);
                    suggestion.causeForSpace(s).highlightGreen.push(common[0]);
                });
                suggestion.help = `Y-Wing: Space ${keySpace.location()} has 2 options, forcing the other in ` +
                    `${a.space.location()} and ${b.space.location()}. Which causes the green option to be left in ` +
                    `the other of the two. ${suggestion.effects[0].space.location()} ` +
                    `can therefore never be a ${suggestion.effects[0].removeOptions[0]}.`
                foundSuggestion = suggestion;
            }
        });

        return foundSuggestion;
    }
}

interface PotentialCorner {
    space: Space;
    intersect: number;
    optionList: number[];
}
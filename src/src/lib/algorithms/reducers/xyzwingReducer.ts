import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";

export class XYZwingReducer extends BaseReducer implements Reducer {
    name: string = 'XYZ-Wing';

    suggest(board: Board): ReduceSuggestion | null {
        for (let space of board.spaces) {
            const suggestion = this.checkSpace(board, space);
            if (suggestion !== null) {
                return suggestion;
            }
        }
        return null
    }

    private checkSpace(board: Board, keySpace: Space): null | ReduceSuggestion {
        if (keySpace.hasValue()) return null;
        if (keySpace.options.optionCount !== 3) return null;

        const options = keySpace.options.toNumberList();

        const corners = board
            .sees(keySpace)
            .filter(s => s.options.optionCount === 2)
            .map(corner => {
                const cornerOptions = corner.options.toNumberList();
                const intersect = ArrayHelpers.intersect(options, cornerOptions);
                return (intersect.length === 2)
                    ? {
                        space: corner,
                        optionList: cornerOptions,
                    }
                    : null;
            })
            .filter(o => o !== null);

        if (corners.length !== 2) return null;

        const [a, b] = corners;

        const commonOptions = ArrayHelpers.intersect(a.optionList, b.optionList);

        if (commonOptions.length !== 1) return null;

        const [commonOption] = commonOptions;

        const candidates = ArrayHelpers.intersect(board.sees(a.space), board.sees(b.space))
            .filter(s => !s.hasValue())
            .filter(s => s.sees(keySpace))
            .filter(s => s !== a.space && s !== b.space && s !== keySpace)
            .filter(s => s.options.hasOpen(commonOption));

        if (candidates.length === 0) return null;

        const suggestion = new ReduceSuggestion(this);

        candidates.forEach(c => this.suggestClose(c, commonOption, suggestion));

        if (!suggestion.hasEffect) return null;

        suggestion.causeForSpace(keySpace).highlightSpace = true;
        [keySpace, a.space, b.space].forEach(s => {
            suggestion.causeForSpace(s).highlightBlue.push(...options.filter(n => n !== commonOption));
            suggestion.causeForSpace(s).highlightGreen.push(commonOption);
        });

        suggestion.help = `XYZ-Wing: Space ${keySpace.location()}, ${a.space.location()} and ${b.space.location()} ` +
            `have 3 options: ${options.join(', ')}. If any of the green options is correct, then the red option must ` +
            `be wrong since it sees all of them. However, if the blue option in ${a.space.location()} or ${b.space.location()} ` +
            `is correct, then ${keySpace.location()} and the other of the two will form a naked pair that sees the red ` +
            `options, therefore the red option cannot be correct.`;

        return suggestion;
    }
}
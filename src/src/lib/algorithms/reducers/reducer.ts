import {Board} from "lib/board";
import {Space} from "lib/space";

export interface Reducer {
    name: string;

    reduceOptions(board: Board): boolean;

    suggest(board: Board): null | ReduceSuggestion
}

export abstract class BaseReducer {
    protected hasReduced: boolean = false;

    reduceOptions(board: Board): boolean {
        this.hasReduced = false;
        this.reduce(board);
        return this.hasReduced;
    }

    suggest(board: Board): null | ReduceSuggestion {
        return null;
    }

    protected reduce(board: Board): void {
        const suggestion = this.suggest(board);
        if (suggestion !== null && suggestion !== undefined && suggestion.hasEffect) {
            suggestion.applyEffects();
            this.hasReduced = true;
        }
    }

    protected suggestClose(space: Space, i: number, suggestion: ReduceSuggestion): boolean {
        if (space.hasValue()) return false;
        if (space.options.hasOpen(i)) {
            suggestion.effectForSpace(space).closeOption(i);
            return true;
        }
        return false;
    }
}

export class ReduceSuggestion {
    readonly causes: ReduceCause[];
    readonly effects: ReduceEffect[];
    help?: string;

    constructor(public reducer: Reducer) {
        this.causes = [];
        this.effects = [];
    }

    get hasEffect(): boolean {
        return this.effects.length > 0;
    }

    effectForSpace(space: Space): ReduceEffect {
        let effect = this.effects.find(effect => effect.space === space);
        if (effect === undefined) {
            effect = new ReduceEffect(space);
            this.effects.push(effect);
        }

        return effect;
    }

    causeForSpace(space: Space): ReduceCause {
        let cause = this.causes.find(effect => effect.space === space);
        if (cause === undefined) {
            cause = new ReduceCause(space);
            this.causes.push(cause);
        }

        return cause;
    }

    applyEffects(): void {
        this.effects.forEach((effect: ReduceEffect) => {
            effect.removeOptions.forEach(n => effect.space.options.close(n));
        });
    }

    isApplied(): boolean {
        for (let effect of this.effects) {
            for (let n of effect.removeOptions) {
                if (!effect.space.hasValue() && effect.space.options.hasOpen(n)) return false;
            }
        }
        return true;
    }
}

export class ReduceCause {
    highlightSpace: boolean = false;
    highlightBlue: number[] = [];
    highlightGreen: number[] = [];

    constructor(public space: Space) {
    }
}

export class ReduceEffect {
    removeOptions: number[] = [];

    constructor(public space: Space) {
    }

    closeOption(n: number): void {
        if (this.removeOptions.indexOf(n) === -1) {
            this.removeOptions.push(n);
        }
    }
}

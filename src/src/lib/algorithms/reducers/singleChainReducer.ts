import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {SinglesChainFactory} from "../chains/singles";
import {Color, SinglesChain} from "../chains/structs";

export class SingleChainReducer extends BaseReducer implements Reducer {
    name: string = 'Singles Chains';
    private readonly chainFactory: SinglesChainFactory = new SinglesChainFactory();

    suggest(board: Board): ReduceSuggestion | null {
        const analysis = this.chainFactory.analyseBoard(board);
        for (let numberAnalysis of analysis.analysis.values()) {
            for (let chain of numberAnalysis.chains) {
                let suggestion = null;
                suggestion ??= this.sameColorTwiceInRange(chain);
                suggestion ??= this.optionsLookingAtBothColors(board, chain);
                if (suggestion !== null) {
                    return suggestion;
                }
            }
        }
        return null;
    }

    private sameColorTwiceInRange(chain: SinglesChain): null | ReduceSuggestion {
        if (chain.colorMap === undefined) return null;

        const spacesByColor: Map<Color, Space[]> = new Map([
            [Color.Green, []],
            [Color.Blue, []],
        ]);
        chain.colorMap.forEach((c, s) => spacesByColor.get(c).push(s));

        for (const [space, color] of chain.colorMap.entries()) {
            // Check if this space sees other spaces of the same color
            const collidesWith = spacesByColor.get(color).find(collider => {
                return collider !== space && collider.sees(space);
            });

            if (collidesWith === undefined) {
                continue;
            }

            const suggestion = new ReduceSuggestion(this);
            suggestion.causeForSpace(space).highlightSpace = true;
            suggestion.causeForSpace(collidesWith).highlightSpace = true;
            spacesByColor
                .get(color)
                .forEach(s => suggestion.effectForSpace(s).closeOption(chain.number));
            spacesByColor
                .get(this.otherColor(color))
                .forEach(s => {
                    suggestion.causeForSpace(s).highlightBlue.push(chain.number);
                    for (let i = 1; i <= 9; i++) {
                        if (i === chain.number) continue;
                        suggestion.effectForSpace(s).closeOption(i);
                    }
                });
            suggestion.help = `There is a singles chain with ${chain.number}'s with the same color twice in the same ` +
                `range (green background), therefore that color must be invalid, and we've marked it red here. ` +
                `The blue options are the only places left for ${chain.number} and they should all be filled.`;

            return suggestion;
        }

        return null;
    }

    private optionsLookingAtBothColors(board: Board, chain: SinglesChain): null | ReduceSuggestion {
        for (let s of board.spaces) {
            if (
                s.hasValue()
                || !chain.colorMap
                || chain.colorMap.has(s) // space is part of chain
                || !s.options.hasOpen(chain.number)
            ) continue;

            const seesColors: Set<Color> = new Set();

            chain.colorMap.forEach((color, space) => {
                if (s.sees(space)) {
                    seesColors.add(color);
                }
            });

            if (seesColors.size === 2) {
                const suggestion = new ReduceSuggestion(this);
                // This space sees both colors!
                this.suggestClose(s, chain.number, suggestion);

                if (suggestion.hasEffect) {
                    suggestion.help = this.helpForLookingAtBothColors();
                    return this.highlightChain(suggestion, chain);
                }
            }
        }

        return null;
    }

    private helpForLookingAtBothColors(): string {
        return `This is a singles chain. Note how the blue and green spaces form pairs in a range. We don't know if ` +
            `all the blue ones are going to win, or all the green ones, but one of them has to be it. However the red ` +
            `space sees both green and blue so it can certainly not be it.`
    }

    private highlightChain(suggestion: ReduceSuggestion, chain: SinglesChain): ReduceSuggestion {
        chain.colorMap.forEach((color, space) => {
            const cause = suggestion.causeForSpace(space);
            if (color === Color.Blue) {
                cause.highlightBlue.push(chain.number);
            }
            if (color === Color.Green) {
                cause.highlightGreen.push(chain.number);
            }
        })
        return suggestion;
    }

    private otherColor(color: Color): Color {
        return color === Color.Blue ? Color.Green : Color.Blue;
    }
}

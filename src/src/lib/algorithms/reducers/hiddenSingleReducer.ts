import {Board} from "lib/board";
import {Space} from "lib/space";
import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {BoardHelpers} from "lib/helpers/boardHelpers";

export class HiddenSingleReducer extends BaseReducer implements Reducer {
    name: string = 'Hidden single';

    suggest(board: Board): null | ReduceSuggestion {
        return BoardHelpers.findRange<ReduceSuggestion>(board, this.suggestFromRange.bind(this));
    }

    private suggestFromRange(_: Board, range: Space[]): null | ReduceSuggestion {
        const map = BoardHelpers.numberToSpacesMap(range);

        for (const [number, spaces] of map) {
            if (spaces.length !== 1) continue;

            const suggestion = new ReduceSuggestion(this);
            for (let i = 1; i <= 9; i++) {
                if (i === number) {
                    suggestion.causeForSpace(spaces[0]).highlightBlue.push(i);
                } else {
                    this.suggestClose(spaces[0], i, suggestion);
                }
            }
            if (suggestion.hasEffect) {
                range.forEach(s => suggestion.causeForSpace(s).highlightSpace = true);
                return suggestion;
            }
        }

        return null
    }
}
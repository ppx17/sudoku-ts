import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {BoardHelpers} from "lib/helpers/boardHelpers";

export class PointingPairReducer extends BaseReducer implements Reducer {
    name = 'Pointing Pair';

    suggest(board: Board): null | ReduceSuggestion {
        return BoardHelpers.findSquare<ReduceSuggestion>(board, this.processSquare.bind(this));
    }

    private processSquare(board: Board, range: Space[]): null | ReduceSuggestion {
        const numberToSpaces = BoardHelpers.numberToSpacesMap(range);

        // Pointing columns
        for (const [n, spaces] of numberToSpaces.entries()) {
            if (spaces.length < 2) continue;

            const xs = new Set<number>(spaces.map((s: Space) => s.x));
            if (xs.size === 1) { // if all options of this number lie on one X-axis
                const x = xs.values().next().value; // get that single X
                const ys = spaces.map((s: Space) => s.y); //get the Y's part of the pair
                const suggestion = new ReduceSuggestion(this);
                for (let y = 0; y < 9; y++) {
                    const s = board.board[y][x]; // check that whole Y-axis
                    if (s.hasValue() || !s.options.hasOpen(n)) continue;
                    if (ys.indexOf(y) >= 0) { // pair of our pair?
                        suggestion.causeForSpace(s).highlightBlue.push(n);
                    } else {
                        this.suggestClose(s, n, suggestion);
                    }
                }
                if (suggestion.hasEffect) return suggestion;
            }
        }

        // Pointing rows
        for (const [n, spaces] of numberToSpaces.entries()) {
            if (spaces.length < 2) continue;

            const ys = new Set<number>(spaces.map((s: Space) => s.y));
            if (ys.size === 1) {
                const y = ys.values().next().value;
                const xs = spaces.map((s: Space) => s.x);
                const suggestion = new ReduceSuggestion(this);
                for (let x = 0; x < 9; x++) {
                    const s = board.board[y][x];
                    if (s.hasValue() || !s.options.hasOpen(n)) continue;
                    if (xs.indexOf(x) >= 0) {
                        suggestion.causeForSpace(s).highlightBlue.push(n);
                    } else {
                        this.suggestClose(s, n, suggestion);
                    }
                }
                if (suggestion.hasEffect) return suggestion;
            }
        }
        return null;
    }
}
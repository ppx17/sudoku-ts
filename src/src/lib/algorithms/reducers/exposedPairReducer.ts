import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {BoardHelpers} from "lib/helpers/boardHelpers";

export class ExposedPairReducer extends BaseReducer implements Reducer {
    name = 'Exposed Pair';

    public suggest(board: Board): null | ReduceSuggestion {
        return BoardHelpers.findRange<ReduceSuggestion>(board, this.processRange.bind(this));
    }

    private processRange(_: Board, spaces: Space[]): null | ReduceSuggestion {
        const pairs = this.pairSpaces(spaces);

        return this.suggestionFromPairedSpaces(pairs, spaces);
    }

    private suggestionFromPairedSpaces(pairs: Map<string, Space[]>, spaces: Space[]): null | ReduceSuggestion {
        for (const [id, pairedSpaces] of pairs) {
            if (pairedSpaces.length !== 2) continue;

            const suggestion = new ReduceSuggestion(this);

            for (const neighbor of spaces) {
                if (neighbor.hasValue()) continue;
                if (neighbor.options.optionList.join('') === id) {
                    suggestion.causeForSpace(neighbor).highlightBlue = [neighbor.options.toNumberList()[0]];
                    suggestion.causeForSpace(neighbor).highlightGreen = [neighbor.options.toNumberList()[1]];
                    continue;
                }

                let pairNumbers = id.split('').map(i => Number(i));

                for (let i of pairNumbers) {
                    this.suggestClose(neighbor, i, suggestion);
                }
            }

            if (suggestion.hasEffect) return suggestion;
        }

        return null;
    }

    private pairSpaces(spaces: Space[]): Map<string, Space[]> {
        const pairs: Map<string, Space[]> = new Map();

        spaces.forEach((s: Space) => {
            if (s.hasValue()) return;
            if (s.options.optionCount === 2) {
                const id = s.options.optionList.join('');
                if (!pairs.has(id)) {
                    pairs.set(id, []);
                }
                pairs.get(id).push(s);
            }
        });

        return pairs;
    }
}
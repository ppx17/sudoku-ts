import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";

export class NeighborReducer extends BaseReducer implements Reducer {
    name = 'Neighbors';

    suggest(board: Board): ReduceSuggestion | null {
        const suggestion = new ReduceSuggestion(this);
        for (let space of board.spaces) {
            if (space.hasValue()) continue;

            board.row(space.y)
                .concat(board.col(space.x))
                .concat(board.square(space.x, space.y))
                .forEach((neighbor: Space) => {
                    if (!neighbor.hasValue()) return;

                    if (this.suggestClose(space, neighbor.getValue(), suggestion)) {
                        suggestion.causeForSpace(neighbor).highlightSpace = true;
                    }
                });

            if (suggestion.hasEffect) return suggestion;
        }

        return null;
    }
}
import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";
import {BoardHelpers} from "lib/helpers/boardHelpers";

abstract class HiddenSetReducer extends BaseReducer {
    protected setSize: number;

    suggest(board: Board): ReduceSuggestion | null {
        return BoardHelpers.findRange<ReduceSuggestion>(board, this.processRange.bind(this));
    }

    protected abstract spacesAreCandidate(spaces: Space[]): boolean;

    protected abstract lateBinding(): Reducer;

    protected processRange(board: Board, spaces: Space[]): null | ReduceSuggestion {
        const map = BoardHelpers.numberToSpacesMap(spaces);

        let candidates: number[] = [];
        map.forEach((spaces: Space[], n: number) => {
            if (this.spacesAreCandidate(spaces)) {
                candidates.push(n);
            }
        });

        if (candidates.length < this.setSize) return null;

        let foundSuggestion: ReduceSuggestion | null = null;
        ArrayHelpers.crossJoinSets<number>(this.setSize, candidates, (...numbers) => {
            if (foundSuggestion !== null) return;

            const spaces = new Set<Space>();
            numbers.forEach(n => map.get(n).forEach(space => spaces.add(space)));

            if (spaces.size !== this.setSize) return;

            const suggestion = new ReduceSuggestion(this.lateBinding());

            for (let space of spaces) {
                for (let i = 1; i <= 9; i++) {
                    if (numbers.indexOf(i) !== -1) {
                        suggestion.causeForSpace(space).highlightBlue.push(i);
                        continue;
                    }
                    this.suggestClose(space, i, suggestion);
                }
            }

            if (suggestion.hasEffect) {
                foundSuggestion = suggestion;
            }
        });

        return foundSuggestion;
    }
}

export class HiddenPairReducer extends HiddenSetReducer implements Reducer {
    name = 'Hidden Pair';
    protected setSize: number = 2;

    protected spacesAreCandidate(spaces: Space[]): boolean {
        return spaces.length === 2;
    }

    protected lateBinding(): Reducer {
        return this;
    }
}

export class HiddenTripletReducer extends HiddenSetReducer implements Reducer {
    name = 'Hidden Triplet';
    protected setSize: number = 3;

    protected spacesAreCandidate(spaces: Space[]): boolean {
        return spaces.length === 2 || spaces.length === 3;
    }

    protected lateBinding(): Reducer {
        return this;
    }
}

export class HiddenQuadReducer extends HiddenSetReducer implements Reducer {
    name = 'Hidden Quad';
    protected setSize: number = 4;

    protected spacesAreCandidate(spaces: Space[]): boolean {
        return spaces.length === 2 || spaces.length === 3 || spaces.length === 4;
    }

    protected lateBinding(): Reducer {
        return this;
    }
}
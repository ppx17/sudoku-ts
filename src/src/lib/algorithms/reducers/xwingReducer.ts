import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {BoardHelpers, BoardRange} from "lib/helpers/boardHelpers";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";

export class XwingReducer extends BaseReducer implements Reducer {
    name = 'X-Wing';

    suggest(board: Board): ReduceSuggestion | null {
        let result = null;
        result ??= this.findXwingInRange(board, BoardRange.Row);
        result ??= this.findXwingInRange(board, BoardRange.Column);
        return result;
    }

    private findXwingInRange(board: Board, range: BoardRange): ReduceSuggestion | null {

        const candidates: Map<number, number[]> = new Map();
        let pos = 0;
        BoardHelpers.forDynamicRange(board, range, (b, spaces) => {
            const map = BoardHelpers.numberToSpacesMap(spaces);
            map.forEach((spaces: Space[], n: number) => {
                if (spaces.length === 2) {
                    if (!candidates.has(n)) {
                        candidates.set(n, []);
                    }
                    candidates.get(n).push(pos);
                }
            })
            pos++;
        });

        for (const [number, positions] of candidates.entries()) {
            if (positions.length !== 2) continue;
            // We're looking for an x-wing on number n

            let foundSuggestion: null | ReduceSuggestion = null;
            ArrayHelpers.crossJoin<number>(positions, (a, b) => {
                if (foundSuggestion !== null) return;
                // Get two ranges with number n in two places in them
                const posA = this.positions(board, number, range, a);
                const posB = this.positions(board, number, range, b);

                // check if those two places align.
                if (!ArrayHelpers.arraysAreEqual(posA, posB)) {
                    return;
                }

                const suggestion = new ReduceSuggestion(this);

                // exclude options outside of the x-wing
                for (let position of posA) {
                    this.getCounterRange(board, range, position).forEach((space: Space, pos: number) => {
                        if (pos === a || pos === b) {
                            suggestion.causeForSpace(space).highlightBlue.push(number);
                        } else {
                            this.suggestClose(space, number, suggestion);
                        }
                    })
                }

                if (suggestion.hasEffect) {
                    this.highlightCauseRanges(positions, board, range, suggestion);
                    foundSuggestion = suggestion;
                }
            });

            if (foundSuggestion !== null) return foundSuggestion;
        }

        return null;
    }

    private highlightCauseRanges(positions: number[], board: Board, range: BoardRange, suggestion: ReduceSuggestion) {
        positions.forEach(p => {
            this.getRange(board, range, p).forEach(s => suggestion.causeForSpace(s).highlightSpace = true);
        });
    }

    private positions(board: Board, n: number, range: BoardRange, position: number): number[] {
        const counterRange = this.getRange(board, range, position)
            .filter(s => !s.hasValue() && s.options.hasOpen(n));

        return (range === BoardRange.Row)
            ? counterRange.map(s => s.x)
            : counterRange.map(s => s.y);
    }

    private getRange(board: Board, range: BoardRange, position: number) {
        return ((range === BoardRange.Row)
            ? board.row(position)
            : board.col(position));
    }

    private getCounterRange(board: Board, range: BoardRange, position: number): Space[] {
        return (range === BoardRange.Row)
            ? board.col(position)
            : board.row(position);
    }
}
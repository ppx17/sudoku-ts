import {BaseReducer, Reducer, ReduceSuggestion} from "./reducer";
import {Board} from "lib/board";
import {Space} from "lib/space";
import {BoardHelpers} from "lib/helpers/boardHelpers";

export class SquareLineReducer extends BaseReducer implements Reducer {
    name = 'Square-Line'

    suggest(board: Board): ReduceSuggestion | null {
        return BoardHelpers.findRange<ReduceSuggestion>(board, this.processRange.bind(this));
    }

    private processRange(board: Board, range: Space[]): ReduceSuggestion | null {
        const map = BoardHelpers.numberToSpacesMap(range);

        for (const [n, spaces] of map.entries()) {
            const suggestion = this.processNumberInRange(board, n, spaces);
            if (suggestion !== null) {
                range.forEach(s => suggestion.causeForSpace(s).highlightSpace = true);
                return suggestion;
            }
        }

        return null;
    }

    private processNumberInRange(board: Board, n: number, spaces: Space[]): ReduceSuggestion | null {
        if (spaces.length === 0 || spaces.length > 3) return null; // More than 3 cannot fit exclusively in a single square

        if (spaces.length === 1) {
            // Only a single place for the number, this should be caught by the SingleOptionFiller
            return this.removeNumberFromSquareExceptSpaces(board, n, spaces);
        }

        const squares = spaces.map(s => `${Math.floor(s.x / 3)}:${Math.floor(s.y / 3)}`);
        const diff = squares.find(square => square !== squares[0]);
        if (diff === undefined) {
            // All places for this line are in a single square
            return this.removeNumberFromSquareExceptSpaces(board, n, spaces);
        }

        return null;
    }

    private removeNumberFromSquareExceptSpaces(board: Board, n: number, spaces: Space[]): null | ReduceSuggestion {
        const suggestion = new ReduceSuggestion(this);
        spaces.forEach(exceptSpace => {
            board.square(exceptSpace.x, exceptSpace.y).forEach(removeSpace => {
                if (spaces.indexOf(removeSpace) != -1) {
                    suggestion.causeForSpace(removeSpace).highlightBlue.push(n);
                } else {
                    this.suggestClose(removeSpace, n, suggestion);
                }
            })
        });

        return suggestion.hasEffect ? suggestion : null;
    }
}
import {Algorithm} from "./algorithms";
import {Board} from "../board";

class Backtracking implements Algorithm {
    private options: Options;

    constructor(options: Options) {
        this.options = options;
    }

    progress(board: Board): boolean {
        return this.solve(board);
    }

    private solve(board: Board): boolean {
        const empty = board.spaces.find(s => !s.hasValue());
        if (empty === undefined) {
            return true;
        }

        for (let i of this.options.options()) {
            if (board.isLegit(empty, i)) {
                empty.setValue(i);
                if (this.solve(board)) {
                    return true;
                } else {
                    empty.setValue(null);
                }
            }
        }

        return false;
    }
}

export class BacktrackingMultipleSolutionFinder {
    private options: Options = new IncreasingOptions();

    numberOfSolutions(board: Board): number {
        return this.solve(board);
    }

    private solve(board: Board, count: number = 0): number {
        const empty = board.spaces.find(s => !s.hasValue());
        if (empty === undefined) {
            return 1 + count;
        }

        for (let i of this.options.options()) {
            if (board.isLegit(empty, i)) {
                empty.setValue(i);
                count = this.solve(board, count);
            }
            if (count > 1) break;
        }
        empty.setValue(null);
        return count;
    }
}

export interface Options {
    options(): number[];
}

export class IncreasingOptions implements Options {
    options(): number[] {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9];
    }
}

class RandomOptions implements Options {
    private readonly _options: IncreasingOptions;

    constructor() {
        this._options = new IncreasingOptions();
    }

    options(): number[] {
        return this.shuffle(this.shuffle(this._options.options()));
    }

    private shuffle(numbers: number[]) {
        return numbers.sort(() => Math.random() - 0.5);
    }
}

export class IncBacktracking extends Backtracking {
    constructor() {
        super(new IncreasingOptions());
    }
}

export class RndBacktracking extends Backtracking {
    constructor() {
        super(new RandomOptions());
    }
}
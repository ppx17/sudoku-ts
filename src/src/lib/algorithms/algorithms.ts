import {Board} from "../board";

export interface Algorithm {
    progress(board: Board): boolean;
}

export class Algorithms {
    public static maxIterations: number = 81 * 9;

    public static apply(board: Board, algorithm: Algorithm) {
        for (let i = 0; i < this.maxIterations; i++) {
            if (!algorithm.progress(board) || board.allSpacesFilled()) {
                return;
            }
        }
        console.warn(`Applied maximum of ${this.maxIterations} iterations of algorithm.`);
    }
}
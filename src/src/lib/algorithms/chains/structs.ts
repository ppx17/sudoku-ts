import {Space} from "lib/space";
import {Board} from "lib/board";
import {NoteColors} from "./xy";

export type ColorMap = Map<Space, Color>;

export interface SingleChainsBoardAnalysis {
    board: Board;
    analysis: Map<number, SinglesChainNumberAnalysis>;
}

export interface SinglesChainNumberAnalysis {
    number: number;
    chains: SinglesChain[];
    links: MetaLink<Color>[];
}

export type ColoredLink = MetaLink<Color>;

export interface SinglesChain extends Chain<ColoredLink> {
    colorMap?: ColorMap;
}

export interface XyChain extends Chain<MetaLink<NoteColors>> {
    colorMap?: Map<Space, NoteColors>;
}

export interface Chain<T extends Link> {
    number: number;
    links: T[];
}

export enum Color {
    Blue = 'blue',
    Green = 'green',
    Uncolored = 'no'
}

export class Link {
    constructor(public a: Space, public b: Space, public number: number) {
    }

    public location(): string {
        return `${this.a.location()} - ${this.b.location()}`;
    }
}

export class MetaLink<T> extends Link {
    metaA: T;
    metaB: T;
}


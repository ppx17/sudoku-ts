import {Space} from "lib/space";
import {Color, Link} from "./structs";

export class Alternate {
    public static color(color: Color): Color {
        return color === Color.Blue ? Color.Green : Color.Blue;
    }
}

export class SpaceToLinkMapFactory {
    public static create<T extends Link>(links: T[]): Map<Space, T[]> {
        const map: Map<Space, T[]> = new Map();

        links.forEach(l => {
            if (!map.has(l.a)) map.set(l.a, []);
            if (!map.has(l.b)) map.set(l.b, []);

            map.get(l.a).push(l);
            map.get(l.b).push(l);
        })

        return map;
    }
}
import {Board} from "lib/board";
import {Color, MetaLink, XyChain} from "./structs";
import {Space} from "lib/space";
import {Alternate, SpaceToLinkMapFactory} from "./helpers";
import {ArrayHelpers} from "lib/helpers/arrayHelpers";

export class XyChainsFactory {

    private pairs: Space[];
    private colorizer: ColorizeChain = new ColorizeChain();

    public analyseBoard(board: Board): XyChain[] {
        this.pairs = board.spaces.filter(s => !s.hasValue() && s.options.optionCount === 2);

        if (this.pairs.length < 2) return [];

        const processed: Set<Space> = new Set();
        const queue: Space[] = [];
        const chains: XyChain[] = [];

        for (let startingSpace of this.pairs) {
            if (processed.has(startingSpace)) continue;

            let chain: XyChain = {
                number: 0,
                links: [],
            };

            queue.push(startingSpace);

            while (queue.length > 0) {
                const space = queue.shift();
                if (processed.has(space)) continue;
                processed.add(space);

                const options = space.options.toNumberList();

                this.seesPairs(space)
                    .filter(seen => seen.options.hasOpen(options[0]) || seen.options.hasOpen(options[1])) // xor
                    .filter(seen => !processed.has(seen))
                    .forEach(seen => {
                        const n = ArrayHelpers.intersect(seen.options.toNumberList(), options)[0];
                        const link = new MetaLink<NoteColors>(space, seen, n);
                        chain.links.push(link);
                        queue.push(seen);
                    });
            }

            if (chain.links.length === 0) {
                continue;
            }

            chains.push(this.colorizer.colorize(chain));
        }

        return chains;
    }

    private seesPairs(target: Space): Space[] {
        return this.pairs.filter(s => s !== target && target.sees(s));
    }
}

class ColorizeChain {
    private map: Map<Space, MetaLink<NoteColors>[]>;
    private colorMap: Map<Space, NoteColors>;
    private queue: MetaLink<NoteColors>[];

    public colorize(chain: XyChain): XyChain {
        this.initializeFields(chain);

        const firstLink = chain.links[0];
        firstLink.metaA = this.initialNoteColors(firstLink.a);
        this.colorMap.set(firstLink.a, firstLink.metaA);

        this.addLinksOfSpaceToQueue(firstLink.a);

        while (this.queue.length > 0) {
            const link: MetaLink<NoteColors> = this.queue.shift();

            // Load colors from map if possible
            this.loadColorsFromMap(link);

            if (this.linkIsColored(link)) continue;

            let source: Space, dest: Space;
            let aIsDestination = this.colorMap.has(link.b);

            [source, dest] = aIsDestination
                ? [link.b, link.a]
                : [link.a, link.b];

            const destColors = this.noteColorsForDest(dest, source);
            this.colorMap.set(dest, destColors);
            this.addLinksOfSpaceToQueue(dest);

            if (aIsDestination) {
                link.metaA = destColors;
            } else {
                link.metaB = destColors;
            }
        }

        chain.colorMap = this.colorMap;

        return chain;
    }

    private linkIsColored(link: MetaLink<NoteColors>) {
        return link.metaA !== undefined && link.metaB !== undefined;
    }

    private loadColorsFromMap(link: MetaLink<NoteColors>) {
        if (link.metaA === undefined && this.colorMap.has(link.a)) {
            link.metaA = this.colorMap.get(link.a);
        }
        if (link.metaB === undefined && this.colorMap.has(link.b)) {
            link.metaB = this.colorMap.get(link.b);
        }
    }

    private initializeFields(chain: XyChain): void {
        this.map = SpaceToLinkMapFactory.create(chain.links);
        this.colorMap = new Map<Space, NoteColors>();
        this.queue = [];
    }

    private addLinksOfSpaceToQueue(space: Space) {
        this.queue.push(...this.map.get(space));
    }

    private initialNoteColors(initialSpace: Space): NoteColors {
        const options = initialSpace.options.toNumberList();
        const spaceColorMap = new NoteColors();
        spaceColorMap.set(options[0], Color.Blue);
        spaceColorMap.set(options[1], Color.Green);

        return spaceColorMap;
    }

    private noteColorsForDest(dest: Space, source: Space): NoteColors {
        const destOptions = dest.options.toNumberList();

        const number = ArrayHelpers.intersect(source.options.toNumberList(), destOptions)[0];
        const destColors = new NoteColors();
        const sourceColors = this.colorMap.get(source);

        const numberColor = sourceColors.get(number);
        destColors.set(number, Alternate.color(numberColor));
        destColors.set(destOptions.filter(n => n !== number)[0], numberColor);

        return destColors;
    }
}

export class XyChainPathValidator {
    private map: Map<Space, MetaLink<NoteColors>[]>;

    constructor(private chain: XyChain) {
        this.map = SpaceToLinkMapFactory.create(chain.links);
    }

    validPath(number: number, source: Space, destinations: Space[]): null | Route {
        // A path is valid when its start and end leg are not made through a weak-link on number,
        // so we must find a route without using them.

        // ensure al destinations are on the chain.
        destinations = destinations.filter(d => this.map.has(d));

        if (this.sourceNotPartOfChain(source) || destinations.length === 0) return null;

        const queue: Route[] = [];
        const route = new Route([source], number);

        this.map.get(source).filter(link => link.number !== number).forEach(link => {
            const next = route.cloneTo(link.a === source ? link.b : link.a, link.number);
            queue.push(next);
        });

        while (queue.length > 0) {
            const route = queue.shift();
            const validNextLinks = this.map.get(route.lastSpace()).filter(link => link.number !== route.number);

            for (const link of validNextLinks) {
                const target = link.a === route.lastSpace() ? link.b : link.a;

                if (destinations.indexOf(target) !== -1 && link.number !== number) {
                    route.visit(target);
                    return route;
                }

                if (route.hasVisited(target)) continue;

                const next = route.cloneTo(link.a === route.lastSpace() ? link.b : link.a, link.number);

                queue.push(next);
            }
        }

        return null;
    }

    private sourceNotPartOfChain(source: Space) {
        return !this.map.has(source);
    }
}

export class Route {
    constructor(private path: Space[], public readonly number: number) {
    }

    lastSpace(): Space {
        return this.path[this.path.length - 1];
    }

    hasVisited(space: Space): boolean {
        return this.path.indexOf(space) > -1;
    }

    visit(space: Space): void {
        this.path.push(space);
    }

    cloneTo(next: Space, number: number): Route {
        const clone = new Route(Array.from(this.path), number);
        clone.visit(next);
        return clone;
    }

    spaces(): Space[] {
        return this.path;
    }
}


export class NoteColors {
    colors: Map<number, Color>

    constructor() {
        this.colors = new Map<number, Color>();
    }

    get(n: number) {
        return this.colors.has(n)
            ? this.colors.get(n)
            : Color.Uncolored;
    }

    set(n: number, color: Color): void {
        this.colors.set(n, color);
    }
}

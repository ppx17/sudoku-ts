import {Board} from "lib/board";
import {Space} from "lib/space";
import {
    Color,
    ColoredLink,
    MetaLink,
    SingleChainsBoardAnalysis,
    SinglesChain,
    SinglesChainNumberAnalysis
} from "./structs";
import {Alternate, SpaceToLinkMapFactory} from "./helpers";
import {BoardHelpers} from "lib/helpers/boardHelpers";

type ColoredLinkMap = Map<Space, ColoredLink[]>;

export class SinglesChainFactory {
    public analyseBoard(board: Board): SingleChainsBoardAnalysis {
        const result: SingleChainsBoardAnalysis = {
            analysis: new Map(),
            board: board
        }

        for (let i = 1; i <= 9; i++) {
            result.analysis.set(i, this.analyseNumber(board, i));
        }

        return result;
    }

    public analyseNumber(board: Board, number: number): SinglesChainNumberAnalysis {
        const links = this.linksFromBoard(board, number);
        const map = SpaceToLinkMapFactory.create(links);

        return {
            links: links,
            chains: this.chains(map, links, number),
            number: number
        };
    }

    private chains(map: ColoredLinkMap, links: ColoredLink[], number: number): SinglesChain[] {
        if (map.size === 0) return [];

        const result: SinglesChain[] = [];
        const processed: Set<ColoredLink> = new Set();

        let queue: ColoredLink[] = [];

        for (let link of links) {
            if (processed.has(link)) {
                continue;
            }
            let chain: SinglesChain = {
                links: [],
                number: number
            };

            queue.push(link);

            while (queue.length > 0) {
                let link = queue.shift();
                if (processed.has(link)) continue;
                chain.links.push(link);
                processed.add(link);
                queue.push(
                    ...map
                        .get(link.a)
                        .concat(map.get(link.b))
                        .filter(l => l !== link && !processed.has(l))
                );
            }

            result.push(this.colorChain(chain, map));
        }

        return result;
    }

    private linksFromBoard(board: Board, number: number): ColoredLink[] {
        const links: Map<string, ColoredLink> = new Map();

        BoardHelpers.forEachRange(board, (b, spaces) => {
            const link = this.linkFromRange(spaces, number);
            if (link === null) return;
            links.set(link.location(), link);
        });

        // We use a map to de-duplicate links that exist in a row/col and square at the same time.
        return Array.from(links.values());
    }

    private linkFromRange(spaces: Space[], number: number): null | ColoredLink {
        const options = spaces.filter(s => !s.hasValue() && s.options.hasOpen(number));
        return options.length === 2
            ? new MetaLink<Color>(options[0], options[1], number)
            : null;
    }

    private colorChain(chain: SinglesChain, map: ColoredLinkMap): SinglesChain {
        return ColorizeSinglesChain.colorize(chain, map);
    }
}

export class ColorizeSinglesChain {
    public static colorize<T extends SinglesChain>(chain: T, map?: ColoredLinkMap): T {
        if (map === undefined) {
            map = SpaceToLinkMapFactory.create(chain.links);
        }

        return this.colorChain(chain, map);
    }

    private static colorChain<T extends SinglesChain>(chain: T, map: ColoredLinkMap): T {
        if (chain.links.length === 0) return chain;
        if (chain.links.length === 1) {
            chain.links[0].metaA = Color.Blue;
            chain.links[0].metaB = Color.Green;
            return chain;
        }

        const colorMap: Map<Space, Color> = new Map();
        const colored: Set<ColoredLink> = new Set();

        let link = chain.links[0];
        link.metaA = Color.Green;
        link.metaB = Color.Blue;
        colorMap.set(link.a, link.metaA);
        colorMap.set(link.b, link.metaB);
        colored.add(link);

        const queue: ColoredLink[] = [];
        queue.push(...map
            .get(link.a)
            .concat(map.get(link.b))
            .filter(l => l !== link && !colored.has(l)));

        while (queue.length > 0) {
            let link = queue.shift();
            if (colored.has(link)) continue;
            colored.add(link);

            queue.push(...map
                .get(link.a)
                .concat(map.get(link.b))
                .filter(l => l !== link && !colored.has(l)));

            const hasA: boolean = colorMap.has(link.a);
            const hasB: boolean = colorMap.has(link.b);

            // Both spaces have a color assigned
            if (hasA && hasB) {
                link.metaA = colorMap.get(link.a);
                link.metaB = colorMap.get(link.b);
                continue;
            }

            // Now we know for sure one of the two is colored already
            if (hasA) {
                link.metaA = colorMap.get(link.a);
                link.metaB = Alternate.color(link.metaA);
                colorMap.set(link.b, link.metaB);
                continue;
            }

            link.metaB = colorMap.get(link.b);
            link.metaA = Alternate.color(link.metaB);
            colorMap.set(link.a, link.metaA);
        }

        chain.colorMap = colorMap;

        return chain;
    }
}
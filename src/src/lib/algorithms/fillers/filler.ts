import {Board} from "lib/board";
import {Space} from "lib/space";

export interface Filler {
    name: string;

    fillOptions(board: Board): boolean;

    resetFilledSomething(): void;

    hasFilledSomething(): boolean;
}

export abstract class BaseFiller {
    private filledSomething: boolean = false;

    public resetFilledSomething(): void {
        this.filledSomething = false;
    }

    public hasFilledSomething(): boolean {
        return this.filledSomething;
    }

    protected fillSpaceWithNumber(board: Board, space: Space, number: number) {
        if (space.hasValue()) return;
        space.setValue(number);
        this.eliminateOption(board.row(space.y), number);
        this.eliminateOption(board.col(space.x), number);
        this.eliminateOption(board.square(space.x, space.y), number);
        this.filledSomething = true;
    }

    private eliminateOption(spaces: Space[], number: number) {
        spaces.forEach(s => s.options.close(number));
    }
}
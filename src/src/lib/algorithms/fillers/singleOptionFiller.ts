import {BaseFiller, Filler} from "./filler";
import {Board} from "lib/board";
import {Space} from "lib/space";

export class SingleOptionFiller extends BaseFiller implements Filler {
    name: string = 'Single option in space';

    fillOptions(board: Board): boolean {
        this.resetFilledSomething();
        board.spaces.forEach((space: Space) => this.processSpace(board, space));
        return this.hasFilledSomething();
    }

    private processSpace(board: Board, space: Space) {
        if (!space.hasValue() && space.options.hasSingleOption) {
            this.fillSpaceWithNumber(board, space, space.options.firstOption);
        }
    }
}
import {LinearGenerator} from "lib/generator/generator";
import {Difficulty, Rating} from "lib/difficulty";
import {Board, BoardSerializer} from "lib/board";
import {Cluster, Worker} from "cluster";
import {FileRepository, Repository} from "./repository";
import * as os from "os";

export class ParallelGenerator {
    private generator: Generator = new Generator();
    private readonly numThreads: number;
    private requiredSpaces: Map<Difficulty, number> = new Map();
    private repository: Repository = new FileRepository();

    constructor(private options: ParallelGeneratorOptions = {}) {
        this.numThreads = this.options.threads ?? require('os').cpus().length;

        this.requiredSpaces.set(Difficulty.Easy, 21);
        this.requiredSpaces.set(Difficulty.Medium, 23);
        this.requiredSpaces.set(Difficulty.Advanced, 23);
        this.requiredSpaces.set(Difficulty.Hard, 24);
        this.requiredSpaces.set(Difficulty.Expert, 24);
    }

    public async generate(numPuzzles: number): Promise<GenerationResult[]> {
        const cluster = require('cluster');

        return new Promise((resolve) => {
            if (cluster.isMaster) {
                this.masterProcess(numPuzzles, cluster, resolve);
            } else {
                this.workerProcess();
            }
        });
    }

    private masterProcess(numPuzzles: number, cluster: Cluster, resolve: (result: GenerationResult[]) => void) {
        this.log(`Master ${process.pid} is running, starting ${this.numThreads} worker threads`);

        // Fork workers.
        for (let i = 0; i < this.numThreads; i++) {
            cluster.fork();
        }

        const result: GenerationResult[] = [];

        cluster.on('exit', (worker: Worker) => {
            this.log(`worker ${worker.process.pid} finished`);

            if (Object.keys(cluster.workers).length === 0) {
                resolve(result);
            }

            this.repository.close();
        });

        this.forEachWorker(cluster, (worker: Worker) => {
            worker.on('message', (message: GenerationResult) => {
                this.log(`Master received a ${message.rating.difficulty} puzzle from ${worker.id}`);
                this.outputResult(message);
                if (result.length >= numPuzzles) {
                    this.stopAllWorkers(cluster);
                }
            });
        });
    }

    private forEachWorker(cluster: Cluster, callable: (worker: Worker) => void): void {
        for (const id in cluster.workers) {
            callable(cluster.workers[id]);
        }
    }

    private stopAllWorkers(cluster: Cluster): void {
        this.forEachWorker(cluster, (worker: Worker) => {
            worker.send('stop');
        })
    }

    private workerProcess() {
        let active = true;

        os.setPriority(15);

        process.on('message', (msg) => {
            console.log(msg);
            if (msg === 'stop') {
                this.log(`${process.pid} got stop signal`);
                active = false;
            }
        });

        const tick = () => {
            const result = this.generator.generate();

            if (result.spacesFilled < this.requiredSpaces.get(result.rating.difficulty) ?? 22) {
                process.send(result);
            }

            if (active) {
                process.nextTick(tick);
            } else {
                process.exit(0);
            }
        }
        process.nextTick(tick);
    }

    private log(message: string): void {
        process.stderr.write(`${message}\n`);
    }

    private outputResult(message: GenerationResult) {
        this.repository.store(message);
    }
}

export interface ParallelGeneratorOptions {
    threads?: number;
}

export class Generator {
    private generator: LinearGenerator = new LinearGenerator();

    public generate(): GenerationResult {
        const boardWithRating = this.generator.generateMaxDifficulty(Difficulty.Expert);
        this.fixateAllFilled(boardWithRating.board);

        return {
            board: boardWithRating.board,
            rating: boardWithRating,
            spacesFilled: boardWithRating.board.spaces.filter(s => s.isFixed()).length,
            steps: boardWithRating.reductions.map(r => r.reducer.name),
            serialized: BoardSerializer.Serialize(boardWithRating.board, {notes: false}),
        }
    }

    private fixateAllFilled(board: Board): Board {
        board.spaces.forEach(s => {
            if (s.hasValue()) {
                s.markFixed(true);
            }
        });
        return board;
    }
}

export interface GenerationResult {
    board: Board;
    rating: Rating;
    spacesFilled: number;
    steps: string[];
    serialized: string;
}
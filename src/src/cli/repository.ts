import {GenerationResult} from "./generator";
import * as fs from "fs";
import {Difficulty} from "lib/difficulty";

export interface Repository {
    store(puzzle: GenerationResult): void;
    close(): void;
}

export class FileRepository implements Repository {
    private handles: Map<Difficulty, FileStorage>;

    constructor() {
        this.handles = new Map();
        this.handles.set(Difficulty.Easy, new FileStorage("puzzles/easy.ndjson"));
        this.handles.set(Difficulty.Medium, new FileStorage("puzzles/medium.ndjson"));
        this.handles.set(Difficulty.Advanced, new FileStorage("puzzles/advanced.ndjson"));
        this.handles.set(Difficulty.Hard, new FileStorage("puzzles/hard.ndjson"));
        this.handles.set(Difficulty.Expert, new FileStorage("puzzles/expert.ndjson"));
    }

    public store(result: GenerationResult): void {
        if (!this.handles.has(result.rating.difficulty)) return;
        this.handles.get(result.rating.difficulty).store(result);
    }

    public close(): void {
        Promise.all(Array.from(this.handles.values()).map(h => h.close()));
    }
}

class FileStorage {
    private fd: number;
    private queue: GenerationResult[] = [];

    constructor(private path: string) {
        fs.open(path, 'a+', "0644", ((err, fd) => {
            if (err) {
                console.error(err);
                return;
            }

            this.fd = fd;

            this.flush();
        }));
    }

    store(result: GenerationResult) {
        this.queue.push(result);
        this.flush();
    }

    close(): Promise<void> {
        return new Promise((ok, err) => {
            if (!this.fd) {
                ok();
            }

            fs.close(this.fd, closeErr => {
                if (closeErr) err(closeErr);
                ok();
            });
        });
    }

    private flush() {
        if (!this.fd) return;
        if (this.queue.length > 0) {
            this.writeResult(this.queue.shift());
        }
    }

    private writeResult(result: GenerationResult): void {
        if (!this.fd) return;
        fs.write(this.fd, this.serializeResult(result), () => {
            this.flush();
        });
    }

    private serializeResult(result: GenerationResult): string {
        return `{"filled": ${result.spacesFilled}, "rating": "${result.rating.difficulty}", ` +
            `"steps": ${JSON.stringify(result.steps)}, "puzzle":${result.serialized}}\n`
    }
}
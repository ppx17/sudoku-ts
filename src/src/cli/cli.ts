import {ParallelGenerator} from "./generator";

class Cli {
    private readonly pg: ParallelGenerator = new ParallelGenerator({threads: 24});

    run(): number {
        this.pg.generate(1000)
            .then(() => {
                console.log('Target number of puzzles reached. stopping.')
            });

        return 0;
    }
}

const cli = new Cli();
cli.run();
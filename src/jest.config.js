module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    coverageReporters: [
        'text-summary',
        'cobertura',
    ],
    reporters: [
        'default',
        'jest-junit',
    ],
    testResultsProcessor: 'jest-junit',
    moduleNameMapper: {
        '^lib/(.*)$': "<rootDir>/src/lib/$1",
    },
};
import {Difficulty, DifficultyComparer, DifficultyRater, DifficultySerializer, Rating} from "lib/difficulty";
import {DifficultyRaterFactory} from "lib/strategies";
import {BoardSerializer} from "lib/board";
import {HiddenSingleReducer} from "lib/algorithms/reducers/hiddenSingleReducer";
import {HiddenPairReducer} from "lib/algorithms/reducers/hiddenSetReducers";
import {SingleChainReducer} from "lib/algorithms/reducers/singleChainReducer";
import {XyChainsReducer} from "lib/algorithms/reducers/xyChainsReducer";

const easy = '{"values":"604300010050040200000709004009002800020007096406008702090204630042600000000090000"}';
const medium = '{"values":"000310700000042000001000080208060370007000002600020804030090500020500060090270401"}';
const advanced = '{"values":"804000000000004005060020000091008000000000270000210000600000700700006309003081064"}';
const hard = '{"values":"009008000000074860070200010040000970100500000003000400000003298007000000050410000"}';

describe("DifficultyRater", () => {
    const sut = DifficultyRaterFactory.create();

    it("rates an easy board", () => {
        const board = BoardSerializer.Deserialize(easy);

        const rating = sut.rate(board);

        expect(rating).toBeInstanceOf(Rating);

        expect(rating.difficulty).toBe(Difficulty.Easy);
        expect(rating.score).toBe(12);

        expect(rating.hardestStrategy.reducer).toBeInstanceOf(HiddenSingleReducer);
    });

    it("rates a medium board", () => {
        const board = BoardSerializer.Deserialize(medium);

        const rating = sut.rate(board);

        expect(rating).toBeInstanceOf(Rating);

        expect(rating.difficulty).toBe(Difficulty.Medium);
        expect(rating.score).toBe(19);

        expect(rating.hardestStrategy.reducer).toBeInstanceOf(HiddenPairReducer);
    });

    it("rates an advanced board", () => {
        const board = BoardSerializer.Deserialize(advanced);

        const rating = sut.rate(board);

        expect(rating).toBeInstanceOf(Rating);

        expect(rating.difficulty).toBe(Difficulty.Advanced);
        expect(rating.score).toBe(73);

        expect(rating.hardestStrategy.reducer).toBeInstanceOf(SingleChainReducer);
    });

    it("rates a hard board", () => {
        const board = BoardSerializer.Deserialize(hard);

        const rating = sut.rate(board);

        expect(rating).toBeInstanceOf(Rating);

        expect(rating.difficulty).toBe(Difficulty.Hard);
        expect(rating.score).toBe(48);

        expect(rating.hardestStrategy.reducer).toBeInstanceOf(XyChainsReducer);
    });
});

describe("DifficultySerializer", () => {
    it("Can serialize to string", () => {
        const str = DifficultySerializer.serialize(Difficulty.Hard);
        expect(str).toBe('Hard');
    });

    it("Can get a enum value back", () => {
        const hard = DifficultySerializer.deserialize('Hard');

        expect(hard).toBe(Difficulty.Hard);
    });

    it("Can ucfirst", () => {
        const hard = DifficultySerializer.deserialize('hard');

        expect(hard).toBe(Difficulty.Hard);
    });

    it("Can handle weird capitalization", () => {
        const hard = DifficultySerializer.deserialize('hArD');

        expect(hard).toBe(Difficulty.Hard);
    });
});

describe("DifficultyComparer", () => {
    const sut = new DifficultyComparer();

    describe("incomparable", () => {
        it('judges comparable difficulties correctly', () => {
            expect(sut.incomparable(Difficulty.Easy)).toBeFalsy();
            expect(sut.incomparable(Difficulty.Medium)).toBeFalsy();
            expect(sut.incomparable(Difficulty.Advanced)).toBeFalsy();
            expect(sut.incomparable(Difficulty.Hard)).toBeFalsy();
            expect(sut.incomparable(Difficulty.Expert)).toBeFalsy();
        });

        it('judges incomparable difficulties correctly', () => {
            expect(sut.incomparable(Difficulty.Unknown)).toBeTruthy();
            expect(sut.incomparable(Difficulty.Invalid)).toBeTruthy();
        });
    });

    describe("lower", () => {
        it('should think that easy is lower than everything but easy', () => {
            expect(sut.lower(Difficulty.Easy, Difficulty.Easy)).toBeFalsy();

            expect(sut.lower(Difficulty.Easy, Difficulty.Medium)).toBeTruthy();
            expect(sut.lower(Difficulty.Easy, Difficulty.Advanced)).toBeTruthy();
            expect(sut.lower(Difficulty.Easy, Difficulty.Hard)).toBeTruthy();
            expect(sut.lower(Difficulty.Easy, Difficulty.Expert)).toBeTruthy();
        });

        it('should think that hard is not lower, except for expert', () => {
            expect(sut.lower(Difficulty.Hard, Difficulty.Easy)).toBeFalsy();
            expect(sut.lower(Difficulty.Hard, Difficulty.Medium)).toBeFalsy();
            expect(sut.lower(Difficulty.Hard, Difficulty.Advanced)).toBeFalsy();
            expect(sut.lower(Difficulty.Hard, Difficulty.Hard)).toBeFalsy();

            expect(sut.lower(Difficulty.Hard, Difficulty.Expert)).toBeTruthy();
        });
    })

    describe("lowerOrEqual", () => {
        it('should think that easy is lowerOrEqual to everything', () => {
            expect(sut.lowerOrEqual(Difficulty.Easy, Difficulty.Easy)).toBeTruthy();
            expect(sut.lowerOrEqual(Difficulty.Easy, Difficulty.Medium)).toBeTruthy();
            expect(sut.lowerOrEqual(Difficulty.Easy, Difficulty.Advanced)).toBeTruthy();
            expect(sut.lowerOrEqual(Difficulty.Easy, Difficulty.Hard)).toBeTruthy();
            expect(sut.lowerOrEqual(Difficulty.Easy, Difficulty.Expert)).toBeTruthy();
        });

        it('should think that hard is not lower, except for expert and itself', () => {
            expect(sut.lowerOrEqual(Difficulty.Hard, Difficulty.Easy)).toBeFalsy();
            expect(sut.lowerOrEqual(Difficulty.Hard, Difficulty.Medium)).toBeFalsy();
            expect(sut.lowerOrEqual(Difficulty.Hard, Difficulty.Advanced)).toBeFalsy();

            expect(sut.lowerOrEqual(Difficulty.Hard, Difficulty.Hard)).toBeTruthy();
            expect(sut.lowerOrEqual(Difficulty.Hard, Difficulty.Expert)).toBeTruthy();
        });
    })
})








































import {Options, Space} from "lib/space";
import {BoardSerializer} from "lib/board";
import {BoardHelpers, BoardRange} from "lib/helpers/boardHelpers";

const board = BoardSerializer.Deserialize('{"values":"123456789456789123789123456214365897365897214897214365531642978642978531978531642"}');

const rows = [
    '123456789',
    '456789123',
    '789123456',
    '214365897',
    '365897214',
    '897214365',
    '531642978',
    '642978531',
    '978531642',
];

const columns = [
    '147238569',
    '258169347',
    '369457128',
    '471382695',
    '582691473',
    '693574281',
    '714823956',
    '825916734',
    '936745812',
];

const squares = [
    '123456789',
    '214365897',
    '531642978',
    '456789123',
    '365897214',
    '642978531',
    '789123456',
    '897214365',
    '978531642',
];

describe('forEachRow', () => {
    it('calls the callback for each row', () => {
        const callback = jest.fn();

        BoardHelpers.forEachRow(board, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let row of rows.keys()) {
            expect(callback.mock.calls[row][1].map((s: Space) => s.getValue()).join('')).toBe(rows[row]);
        }
    });
});

describe('forEachColumn', () => {
    it('calls the callback for each column', () => {
        const callback = jest.fn();

        BoardHelpers.forEachColumn(board, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let column of columns.keys()) {
            expect(callback.mock.calls[column][1].map((s: Space) => s.getValue()).join('')).toBe(columns[column]);
        }
    });
});

describe('forEachSquare', () => {
    it('calls the callback for each square', () => {
        const callback = jest.fn();

        BoardHelpers.forEachSquare(board, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let square of squares.keys()) {
            expect(callback.mock.calls[square][1].map((s: Space) => s.getValue()).join('')).toBe(squares[square]);
        }
    });
});

describe('forAllRanges', () => {
    it('calls the callback for rows, columns and squares sequentially', () => {
        const callback = jest.fn();

        BoardHelpers.forEachRange(board, callback);

        expect(callback).toBeCalledTimes(27);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let row of rows.keys()) {
            expect(callback.mock.calls[row][1].map((s: Space) => s.getValue()).join('')).toBe(rows[row]);
        }
        for (let column of columns.keys()) {
            expect(callback.mock.calls[9 + column][1].map((s: Space) => s.getValue()).join('')).toBe(columns[column]);
        }
        for (let square of squares.keys()) {
            expect(callback.mock.calls[18 + square][1].map((s: Space) => s.getValue()).join('')).toBe(squares[square]);
        }
    });
});

describe('forDynamicRange', () => {
    it('calls the callback for rows', () => {
        const callback = jest.fn();

        BoardHelpers.forDynamicRange(board, BoardRange.Row, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let row of rows.keys()) {
            expect(callback.mock.calls[row][1].map((s: Space) => s.getValue()).join('')).toBe(rows[row]);
        }
    });

    it('calls the callback for columns', () => {
        const callback = jest.fn();

        BoardHelpers.forDynamicRange(board, BoardRange.Column, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let column of columns.keys()) {
            expect(callback.mock.calls[column][1].map((s: Space) => s.getValue()).join('')).toBe(columns[column]);
        }
    });

    it('calls the callback for squares', () => {
        const callback = jest.fn();

        BoardHelpers.forDynamicRange(board, BoardRange.Square, callback);

        expect(callback).toBeCalledTimes(9);
        expect(callback.mock.calls[0][0]).toBe(board);

        for (let square of squares.keys()) {
            expect(callback.mock.calls[square][1].map((s: Space) => s.getValue()).join('')).toBe(squares[square]);
        }
    });
});

describe('numberToSpacesMap', () => {
    it('counts options correctly', () => {
        const spaces = [
            new Space(0, 1, new Options(false)),
            new Space(0, 1, new Options(false)),
        ];

        spaces[0].options.fromNumberList([1, 2, 3]);
        spaces[1].options.fromNumberList([2, 3, 4]);

        const map = BoardHelpers.numberToSpacesMap(spaces);

        expect(map.get(1)).toHaveLength(1);
        expect(map.get(2)).toHaveLength(2);
        expect(map.get(3)).toHaveLength(2);
        expect(map.get(4)).toHaveLength(1);

        expect(map.get(1)[0]).toBe(spaces[0]);
        expect(map.get(4)[0]).toBe(spaces[1]);
    });
});

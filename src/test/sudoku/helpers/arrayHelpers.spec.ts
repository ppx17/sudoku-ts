import {ArrayHelpers} from "lib/helpers/arrayHelpers";

describe('crossJoin', () => {
    it('does nothing when the list has less than two items', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoin([1], fn);

        expect(fn.mock.calls.length).toBe(0);
    });

    it('calls the callback once for two options', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoin([5, 6], fn);

        expect(fn.mock.calls.length).toBe(1);
        expect(fn.mock.calls[0][0]).toBe(5);
        expect(fn.mock.calls[0][1]).toBe(6);
    });

    it('cross joins without having the same element pair twice', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoin([4, 5, 6, 7], fn);

        // 45, 46, 47, 56, 57, 67
        expect(fn.mock.calls.length).toBe(6);
    });

    it('can handle a 0 in the array', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoin([0, 1], fn);

        expect(fn.mock.calls.length).toBe(1);
        expect(fn.mock.calls[0][0]).toBe(0);
        expect(fn.mock.calls[0][1]).toBe(1);
    });
});

describe('crossJoinTriplet', () => {
    it('does nothing when the list has less than three items', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoinTriplet([1, 2], fn);

        expect(fn.mock.calls.length).toBe(0);
    });

    it('calls the callback once for three options', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoinTriplet([5, 6, 7], fn);

        expect(fn.mock.calls.length).toBe(1);
        expect(fn.mock.calls[0][0]).toBe(5);
        expect(fn.mock.calls[0][1]).toBe(6);
        expect(fn.mock.calls[0][2]).toBe(7);
    });

    it('cross joins without having the same element triplet twice', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoinTriplet([4, 5, 6, 7, 8], fn);

        // 456, 457, 458, 467, 468, 478, 567, 568, 578, 678
        expect(fn.mock.calls.length).toBe(10);
    });

    it('can handle a 0 in the array', () => {
        const fn = jest.fn();

        ArrayHelpers.crossJoinTriplet([0, 1, 2], fn);

        expect(fn.mock.calls.length).toBe(1);
        expect(fn.mock.calls[0][0]).toBe(0);
        expect(fn.mock.calls[0][1]).toBe(1);
        expect(fn.mock.calls[0][2]).toBe(2);
    });
});
import {Board, RenderedBoard} from "lib/board";
import {InputSpace, Space} from "lib/space";

describe("Board", () => {
    const sut = new Board();
    sut.generateSpaces();

    describe('.generateSpaces()', () => {
        it('creates regular Space instances', () => {
            expect(sut.spaces).toHaveLength(81);
            expect(sut.spaces[0]).toBeInstanceOf(Space);
        });
    });

    describe(".sees()", () => {
        const target = sut.board[1][1];
        const seen = sut.sees(target);

        it('should not see the target itself', () => {
            expect(seen).not.toContain(target);
        });

        it('should see 8 + 6 + 6 = 20 spaces', () => {
            expect(seen).toHaveLength(20);
        });

        it('should see all spaces from the square', () => {
            expect(seen).toContain(sut.board[0][0]);
            expect(seen).toContain(sut.board[0][1]);
            expect(seen).toContain(sut.board[0][2]);

            expect(seen).toContain(sut.board[1][0]);
            // Not itself
            expect(seen).toContain(sut.board[1][2]);

            expect(seen).toContain(sut.board[2][0]);
            expect(seen).toContain(sut.board[2][1]);
            expect(seen).toContain(sut.board[2][2]);
        });

        it('should see all spaces from the row', () => {
            expect(seen).toContain(sut.board[1][0]);
            // Not itself
            expect(seen).toContain(sut.board[1][2]);

            expect(seen).toContain(sut.board[1][3]);
            expect(seen).toContain(sut.board[1][4]);
            expect(seen).toContain(sut.board[1][5]);

            expect(seen).toContain(sut.board[1][6]);
            expect(seen).toContain(sut.board[1][7]);
            expect(seen).toContain(sut.board[1][8]);
        });

        it('should see all spaces from the column', () => {
            expect(seen).toContain(sut.board[0][1]);
            // Not itself
            expect(seen).toContain(sut.board[2][1]);

            expect(seen).toContain(sut.board[3][1]);
            expect(seen).toContain(sut.board[4][1]);
            expect(seen).toContain(sut.board[5][1]);

            expect(seen).toContain(sut.board[6][1]);
            expect(seen).toContain(sut.board[7][1]);
            expect(seen).toContain(sut.board[8][1]);
        });
    });

    describe('.space()', () => {
        it('should return null for an invalid label', () => {
            expect(sut.space('Z1')).toBeNull();
            expect(sut.space('A0')).toBeNull();
            expect(sut.space('J1')).toBeNull();
            expect(sut.space('A10')).toBeNull();
            expect(sut.space('')).toBeNull();
        });

        it('should return the corresponding space for a valid label', () => {
            expect(sut.space('A1')).toBe(sut.board[0][0]);
            expect(sut.space('I1')).toBe(sut.board[8][0]);
            expect(sut.space('A9')).toBe(sut.board[0][8]);
            expect(sut.space('I9')).toBe(sut.board[8][8]);
            expect(sut.space('E5')).toBe(sut.board[4][4]);
        })
    });

    describe('.square()', () => {
        it('should return the first square when given the A1 coordinates.', () => {
            const result = sut.square(0, 0); // A1, should give top most square

            expect(result).toHaveLength(9);
            expect(result[0]).toBe(sut.board[0][0]); // validate top-left
            expect(result[8]).toBe(sut.board[2][2]); // validate bottom-right
        });

        it('should return the first square when given the C3 coordinates.', () => {
            const result = sut.square(2, 2); // Top most space, should give top most square

            expect(result).toHaveLength(9);
            expect(result[0]).toBe(sut.board[0][0]); // validate top-left
            expect(result[8]).toBe(sut.board[2][2]); // validate bottom-right
        });

        it('should return the last square when given the I9 coordinates.', () => {
            const result = sut.square(8, 8); // I9 space should give last square

            expect(result).toHaveLength(9);
            expect(result[0]).toBe(sut.board[6][6]); // validate top-left
            expect(result[8]).toBe(sut.board[8][8]); // validate bottom-right
        });
    })
});

describe("RenderedBoard", () => {
    it('creates InputSpace instances', () => {
        const sut = new RenderedBoard();
        sut.generateSpaces();
        expect(sut.spaces).toHaveLength(81);
        expect(sut.spaces[0]).toBeInstanceOf(InputSpace);
    })
});
import {NoteColors, Route, XyChainPathValidator, XyChainsFactory} from "lib/algorithms/chains/xy";
import {BoardSerializer} from "lib/board";

const puzzle = '{"values":"080103070090506000001408020578241639143659782926837451037905200000304097419782060","notes":["26","123456789","245","123456789","29","123456789","59","123456789","456","37","123456789","24","123456789","27","123456789","18","14","348","37","56","123456789","123456789","79","123456789","359","123456789","356","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","68","123456789","123456789","123456789","16","123456789","123456789","14","48","268","56","25","123456789","16","123456789","18","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","35","123456789","35"]}';

describe('XyChainsFactory', () => {

    const board = BoardSerializer.Deserialize(puzzle);

    const sut = new XyChainsFactory();
    it('makes a chain', () => {
        const result = sut.analyseBoard(board);

        expect(result).toHaveLength(1);

        const chain = result[0];

        result.forEach(chain => {
            chain.links.forEach(l => {
                expect(l.metaA).toBeInstanceOf(NoteColors)
            });
        });

        expect(chain.links).toHaveLength(31);

        expect(chain.colorMap.has(board.board[2][1])).toBeTruthy();
        expect(chain.colorMap.has(board.board[8][8])).toBeTruthy();
    });
});

describe('XyChainPathValidator', () => {
    const board = BoardSerializer.Deserialize(puzzle);
    const factory = new XyChainsFactory();
    const result = factory.analyseBoard(board);
    const chain = result[0];
    const sut = new XyChainPathValidator(chain);

    it('recognizes a valid chain', () => {
        expect(sut.validPath(5, board.board[0][6], [board.board[2][1]])).toBeInstanceOf(Route);
    });

    it('recognizes an invalid chain', () => {
        expect(sut.validPath(5, board.board[0][6], [board.board[8][8]])).toBeNull();
    });
})


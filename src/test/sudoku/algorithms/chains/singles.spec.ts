import {BoardSerializer} from "lib/board";
import {SinglesChainFactory} from "lib/algorithms/chains/singles";

const puzzle = '{"values":"070320651000910700000007900300476189000283576786591423004732865200049317037100294","notes":["489","","89","","","48","6","","","4568","246","238","5689","","458","","34","28","1458","124","1238","68","56","4578","","34","28","","25","25","","7","","1","8","","149","149","19","27","","3","","7","367","7","8","","5","3579","1","4","","3","19","19","","7","3","257","","","5","","56","58","68","4567","","3","17","57","68","356","","","56","58","2","9",""]}';

describe("ChainAnalyzer", () => {
    const board = BoardSerializer.Deserialize(puzzle);
    const sut = new SinglesChainFactory();
    it('Generates links', () => {
        const result = sut.analyseNumber(board, 8);

        expect(result.links).toHaveLength(6);
        expect(result.chains).toHaveLength(2);

        expect(result.chains[0].links).toHaveLength(5);
        expect(result.chains[1].links).toHaveLength(1);
    });
});
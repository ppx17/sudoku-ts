import {BoardSerializer} from "lib/board";
import {OptionReducerFactory} from "lib/strategies";

const puzzles = [
    '{"values":"000000001600900400040063500000050340060800010100400900000080000809000603002390050","fixed":"000000001100100100010011100000010110010100010100100100000010000101000101001110010"}',
    '{"values":"000900030073000400000010006600000000000000040008624709900030000200070900000140005","fixed":"000100010011000100000010001100000000000000010001111101100010000100010100000110001"}',
];

describe("Play a game and validate the result", () => {

    const sut = OptionReducerFactory.create();

    it("Doesn't deviate from the solution", () => {
        for (const puzzle of puzzles) {
            const board = BoardSerializer.Deserialize(puzzle);
            const solution = board.solution();

            sut.progress(board);

            for (const space of board.spaces) {
                if (space.isFixed() || !space.hasValue()) continue;

                const expected = solution.board[space.y][space.x].getValue();
                expect(space.getValue()).toStrictEqual(expected);
            }
        }
    })
});




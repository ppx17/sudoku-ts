import {BoardSerializer} from "lib/board";
import {YwingReducer} from "lib/algorithms/reducers/ywingReducer";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const puzzle = '{"values":"900240000050690231020050090090700320002935607070002900069020073510079062207086009","notes":["","38","1368","","","1378","57","58","568","478","","48","","","78","","","","13678","","1368","18","","1378","47","","468","1468","","14568","","16","48","","","458","148","48","1248","","","","","148","","13468","","134568","48","16","","","1458","458","48","","","145","","14","1458","","","","","348","34","","","48","","","","34","","1345","","","145","45",""]}';

describe('YwingReducer', () => {
    const sut = new YwingReducer();

    it('reduces a y-wing', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        sut.reduceOptions(board);

        expect(getOptions(board, 2, 7)).not.toContain(4);
        expect(getOptions(board, 2, 7)).toContain(3);
        expect(getOptions(board, 2, 7)).toContain(8);
    });

    it('suggests a y-wing', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([4]);
        expect(suggestion.effects[0].space).toBe(board.board[7][2]);
    })
});
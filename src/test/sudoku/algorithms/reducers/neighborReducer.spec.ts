import {NeighborReducer} from "lib/algorithms/reducers/neighborReducer";
import {BoardSerializer} from "lib/board";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

// +---+---+---+
// |   |   |   |
// |54 | 89|   |
// |7  |3  |1 5|
// |---|---|---|
// | 53|   |   |
// |  7|   |   |
// | 98|   |   |
// |---|---|---|
// | 1 |   |   |
// |   |   |   |
// |  5|   |   |
// |---|---|---|

const puzzle = '{"values":"000000000540089000700300105053000000007000000098000000010000000000000000005600000","notes":["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]}';

describe('NeighborReducer', () => {

    const sut = new NeighborReducer();

    it('doesn\'t remove valid options', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        board.board[0][0].options.openAllOptions();

        sut.reduceOptions(board);
        expect(getOptions(board, 0, 0)).toContain(1);
        expect(getOptions(board, 0, 0)).toContain(2);
        expect(getOptions(board, 0, 0)).toContain(3);
        expect(getOptions(board, 0, 0)).toContain(6);
        expect(getOptions(board, 0, 0)).toContain(8);
        expect(getOptions(board, 0, 0)).toContain(9);
    });

    it('removes neighbors from column', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        board.board[0][1].options.openAllOptions();

        expect(getOptions(board, 1, 0)).toContain(1);
        expect(getOptions(board, 1, 0)).toContain(9);

        sut.reduceOptions(board);

        expect(getOptions(board, 1, 0)).not.toContain(1);
        expect(getOptions(board, 1, 0)).not.toContain(9);
    });

    it('removes neighbors from row', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        board.board[1][2].options.openAllOptions();

        expect(getOptions(board, 2, 1)).toContain(9);

        sut.reduceOptions(board);

        expect(getOptions(board, 2, 1)).not.toContain(9);
    });

    it('removes neighbors from square', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        board.board[0][0].options.openAllOptions();

        expect(getOptions(board, 0, 0)).toContain(5);
        expect(getOptions(board, 0, 0)).toContain(4);
        expect(getOptions(board, 0, 0)).toContain(7);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 0)).not.toContain(5);
        expect(getOptions(board, 0, 0)).not.toContain(4);
        expect(getOptions(board, 0, 0)).not.toContain(7);
    });

    it('can create a suggestion', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        board.board[0][0].options.openAllOptions();

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects[0].space).toBe(board.board[0][0]);

        const causeSpaces = suggestion.causes.map(c => c.space);
        expect(causeSpaces).toHaveLength(3);
        expect(causeSpaces).toContain(board.board[1][0]);
        expect(causeSpaces).toContain(board.board[2][0]);
        expect(causeSpaces).toContain(board.board[1][1]);

        suggestion.causes.forEach(c => expect(c.highlightSpace).toBeTruthy());

        expect(suggestion.effects[0].removeOptions).toStrictEqual([5, 7, 4]);
    })
})
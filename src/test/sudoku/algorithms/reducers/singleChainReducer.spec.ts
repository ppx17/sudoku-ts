import {BoardSerializer} from "lib/board";
import {SingleChainReducer} from "lib/algorithms/reducers/singleChainReducer";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const puzzle = '{"values":"070320651000910700000007900300476189000283576786591423004732865200049317037100294","notes":["489","","89","","","48","6","","","4568","246","238","5689","","458","","34","28","1458","124","1238","68","56","4578","","34","28","","25","25","","7","","1","8","","149","149","19","27","","3","","7","367","7","8","","5","3579","1","4","","3","19","19","","7","3","257","","","5","","56","58","68","4567","","3","17","57","68","356","","","56","58","2","9",""]}';
const colorTwiceInRangePuzzle = '{"values":"007083600039706800826419753640190387080367000073048060390870026764900138208630970","fixed":"000000000000000000000000000000000000000000000000000000000000000000000000000000000","notes":["145","15","","25","","","","149","1249","145","","","","25","","","14","124","","","","","","","","","","","","25","","","25","","","","159","","12","","","","245","149","1459","19","","","25","","","25","","19","","","15","","","145","45","","","","","","","25","25","","","","","15","","","","14","","","45"]}';

describe("SingleChainReducer", () => {
    const sut = new SingleChainReducer();
    it("reduces an option that sees both colors in a chain", () => {
        const board = BoardSerializer.Deserialize(puzzle);
        sut.reduceOptions(board);

        expect(getOptions(board, 0, 2)).not.toContain(8);
    });

    it('suggests an option that sees both colors in a chain', () => {
        const board = BoardSerializer.Deserialize(puzzle);
        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].space).toBe(board.board[2][0]);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([8]);

        expect(suggestion.causes).toHaveLength(5);
    })

    it('suggests hidden singles, same color twice in range', () => {
        const board = BoardSerializer.Deserialize(colorTwiceInRangePuzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(16);

        expect(suggestion.causeForSpace(board.board[1][0]).highlightSpace).toBeTruthy();
        expect(suggestion.causeForSpace(board.board[4][0]).highlightSpace).toBeTruthy();
    })
});
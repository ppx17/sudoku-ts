import {SquareLineReducer} from "lib/algorithms/reducers/squareLineReducer";
import {BoardSerializer} from "lib/board";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const puzzle = '{"values":"901500046425090081860010020502000000019000460600000002196040253200060817000001694","notes":["","37","","","28","28","37","","","","","","367","","367","37","","","","","37","347","","347","59","","59","","3478","","1469","378","469","19","37","89","37","","","2378","23578","23578","","","58","","3478","3478","149","3578","49","159","37","","","","","78","","78","","","","","345","34","39","","359","","","","37","3578","378","23","235","","","",""]}';

describe('SquareLineReducer', () => {
    const sut = new SquareLineReducer();

    it('Reduces the 7 from the center square due to it being part of the center column', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        sut.reduceOptions(board);

        // These are the only places where the 7 can go in column 4.
        expect(getOptions(board, 4, 3)).toContain(7);
        expect(getOptions(board, 4, 4)).toContain(7);
        expect(getOptions(board, 4, 5)).toContain(7);

        // Since the 7 goes in one of the above options, these options in the same square are out.
        expect(getOptions(board, 3, 4)).not.toContain(7);
        expect(getOptions(board, 5, 4)).not.toContain(7);
    });

    it('Suggests the 7 from the center square due to it being part of the center column', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(2);
        expect(suggestion.effects[0].space).toBe(board.board[4][3]);
        expect(suggestion.effects[1].space).toBe(board.board[4][5]);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([7]);
        expect(suggestion.effects[1].removeOptions).toStrictEqual([7]);
    });
});
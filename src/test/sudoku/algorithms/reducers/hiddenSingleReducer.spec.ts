import {BoardSerializer} from "lib/board";
import {HiddenSingleReducer} from "lib/algorithms/reducers/hiddenSingleReducer";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";


describe('HiddenSingleReducer', () => {
    const puzzle = '{"values":"090006801102938007680714002906000000520000470070095020860009000000807000010000000","notes":["347","","347","25","25","","","34","","","45","","59","","","56","456","","","","35","","","4","359","359","","9","34","","1234","2478","123","135","1358","358","","","18","136","68","13","","","3689","34","","18","1346","","","136","","368","","","3457","12345","245","","12357","1345","345","234","345","3459","","2456","","123569","134569","34569","2347","","34579","23456","2456","23","235679","345689","345689"]}';
    const board = BoardSerializer.Deserialize(puzzle);
    const sut = new HiddenSingleReducer();

    it('reduces hidden singles', () => {
        const b = board.clone();
        sut.reduceOptions(b);

        expect(getOptions(b, 4, 3)).toContain(7);
        expect(getOptions(b, 4, 3)).toHaveLength(1);
    });

    it('suggests hidden singles', () => {
        const b = board.clone();
        const suggestion = sut.suggest(b);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].space).toBe(b.board[3][4]);
        expect(suggestion.effects[0].removeOptions).toHaveLength(3);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([2, 4, 8]);

        expect(suggestion.causes).toHaveLength(9);
        const affectedCause = suggestion.causeForSpace(b.board[3][4]);
        expect(affectedCause).not.toBeNull();
        expect(affectedCause.highlightBlue).toHaveLength(1);
        expect(affectedCause.highlightBlue).toContain(7);
    })
})
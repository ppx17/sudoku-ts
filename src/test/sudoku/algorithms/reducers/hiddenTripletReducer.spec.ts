import {BoardSerializer} from "lib/board";
import {getOptions} from "./helpers";
import {HiddenTripletReducer} from "lib/algorithms/reducers/hiddenSetReducers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const puzzle = '{"values":"000001030231090000065003100678924300103050006000136700009360570006019843300000000","notes":["4789","489","47","245678","478","12578","2469","","245789","","","","45678","","578","46","568","4578","4789","","","2478","478","","","289","24789","","","","","","","","15","15","","249","","78","","78","249","289","","459","2459","24","","","","","2589","24589","48","1248","","","","28","","","12","57","25","","257","","","","","","","12458","247","24578","478","2578","269","1269","129"]}';

describe('HiddenTripletReducer', () => {
    const sut = new HiddenTripletReducer();

    it('reduces a row triplet', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        sut.reduceOptions(board);

        const a = getOptions(board, 3, 0);
        expect(a).not.toContain(4);
        expect(a).not.toContain(7);
        expect(a).not.toContain(8);
        expect(a).toContain(2);
        expect(a).toContain(5);
        expect(a).toContain(6);

        const b = getOptions(board, 6, 0);
        expect(b).not.toContain(4);
        expect(b).not.toContain(9);
        expect(b).toContain(2);
        expect(b).toContain(6);

        const c = getOptions(board, 8, 0);
        expect(c).not.toContain(4);
        expect(c).not.toContain(7);
        expect(c).not.toContain(8);
        expect(c).not.toContain(9);
        expect(c).toContain(2);
        expect(c).toContain(5);
    });

    it('suggests a triplet', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.causes.map(c => c.space.location()).join(',')).toBe('A4,A7,A9');
    });
});
import {BoardSerializer} from "lib/board";
import {XYZwingReducer} from "lib/algorithms/reducers/xyzwingReducer";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const puzzle = '{"values":"040000000680300010032700900000040705050000420423675090204530000800000050315006000","notes":["1579","","179","1289","125689","1289","23568","3678","23678","","","79","","259","249","25","","247","15","","","","1568","148","","468","468","19","69","1689","1289","","12389","","36","","179","","16789","189","189","1389","","","36","","","","","","","18","","18","259","679","","12589","","1789","168","68","1689","","679","69","1249","129","1279","1236","","123469","2359","","59","2489","289","","28","478","24789"]}';
const puzzleSuggestionNotSeeingKey = '{"values":"080103070090506000001408020578241639143659782926837451037905200000304097419782060","notes":["26","123456789","45","123456789","29","123456789","9","123456789","456","37","123456789","24","123456789","27","123456789","18","14","38","37","56","123456789","123456789","79","123456789","39","123456789","56","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","68","123456789","123456789","123456789","16","123456789","123456789","14","48","268","56","25","123456789","16","123456789","18","123456789","123456789","123456789","123456789","123456789","123456789","123456789","123456789","35","123456789","35"]}';

describe('XYZwingReducer', () => {
    const sut = new XYZwingReducer();

    it('reduces a xyz-wing', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        sut.reduceOptions(board);

        expect(getOptions(board, 6, 8)).toContain(2);
        expect(getOptions(board, 6, 8)).not.toContain(8);
    });

    it('suggests a xyz-wing', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([8]);
        expect(suggestion.effects[0].space).toBe(board.board[8][6]);
    })

    it('does not suggest an xyz-wing when the suggestion can not see the key-space', () => {
        const board = BoardSerializer.Deserialize(puzzleSuggestionNotSeeingKey);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeNull();
    })
});
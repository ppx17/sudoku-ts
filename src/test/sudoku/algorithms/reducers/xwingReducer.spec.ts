import {BoardSerializer} from "lib/board";
import {XwingReducer} from "lib/algorithms/reducers/xwingReducer";
import {getOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

describe('XwingReducer', () => {
    const sut = new XwingReducer();

    it('reduces a row-wise x-wing', () => {
        const board = BoardSerializer.Deserialize('{"values":"608090107079300026000067000000603070706000200080700060805030742047008610102070908","notes":["","235","","245","","245","","35","","45","","","","1458","145","458","","","2345","1235","134","2458","","","3458","89","3459","2459","1259","14","","12458","","458","","1459","","1359","","14589","1458","1459","","89","13459","23459","","134","","1245","12459","345","","13459","","69","","19","","169","","","","39","","","259","25","","","","35","","36","","45","","456","","35",""]}');

        expect(getOptions(board, 0, 1)).toContain(4);   // outside x-wing
        expect(getOptions(board, 3, 0)).toContain(4);   // part of x-wing
        expect(getOptions(board, 3, 2)).toContain(4);   // sees of x-wing

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 1)).toContain(4);     // outside x-wing
        expect(getOptions(board, 3, 0)).toContain(4);     // part of x-wing
        expect(getOptions(board, 3, 2)).not.toContain(4); // sees of x-wing
    });

    it('reduces another row-wise x-wing', () => {
        const board = BoardSerializer.Deserialize('{"values":"087005060002700000000080007008090100093001056000057000006219548149578600825364001","notes":["349","","147","149","234","","2349","","2349","34569","1356","","","34","36","3489","1389","3459","34569","1356","14","1469","","236","2349","1239","","24567","567","","46","","236","","237","234","247","","","48","24","","2478","","","246","16","14","468","","","23489","2389","2349","37","37","","","","","357","","","","","","","","","","23","23","","","5","","","","79","79","19"]}');

        expect(getOptions(board, 0, 1)).toContain(5);
        expect(getOptions(board, 1, 1)).toContain(5);
        expect(getOptions(board, 0, 2)).toContain(5);
        expect(getOptions(board, 1, 2)).toContain(5);
        expect(getOptions(board, 0, 3)).toContain(5);
        expect(getOptions(board, 1, 3)).toContain(5);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 1)).not.toContain(5);
        expect(getOptions(board, 1, 1)).not.toContain(5);
        expect(getOptions(board, 0, 2)).toContain(5);
        expect(getOptions(board, 1, 2)).toContain(5);
        expect(getOptions(board, 0, 3)).toContain(5);
        expect(getOptions(board, 1, 3)).toContain(5);
    });

    it('suggests a row-wise x-wing', () => {
        const board = BoardSerializer.Deserialize('{"values":"608090107079300026000067000000603070706000200080700060805030742047008610102070908","notes":["","235","","245","","245","","35","","45","","","","1458","145","458","","","2345","1235","134","2458","","","3458","89","3459","2459","1259","14","","12458","","458","","1459","","1359","","14589","1458","1459","","89","13459","23459","","134","","1245","12459","345","","13459","","69","","19","","169","","","","39","","","259","25","","","","35","","36","","45","","456","","35",""]}');

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(5);
        expect(suggestion.effects.map(s => s.space.location()).join(',')).toBe('C4,E4,B6,E6,F6');

        expect(suggestion.causes.filter(c => c.highlightBlue.length > 0).map(c => c.space.location()).join(','))
            .toBe('A4,I4,A6,I6');
    })
});
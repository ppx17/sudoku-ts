import {BoardSerializer} from "lib/board";
import {getOptions} from "./helpers";
import {HiddenQuadReducer} from "lib/algorithms/reducers/hiddenSetReducers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

const columnQuadPuzzle = '{"values":"005407810000102700001056002107000408402001009806000201600010000013908000004703100","notes":["239","26","","","39","","","","36","39","46","89","","389","","","456","456","37","478","","38","","69","39","49","","","359","","2356","23","59","","356","","","35","","3568","378","","56","3567","","","359","","35","347","459","","357","","","278","89","25","","45","39","2489","347","257","","","","246","","56","24","47","259","28","","","26","","","289","56"]}';
const squareQuadPuzzle = '{"values":"901500046425090081860010020502000000019000460600000002196040253200060817000001694","notes":["","37","","","2378","2378","37","","","","","","367","","367","37","","","","","37","347","","347","3579","","59","","3478","","1346789","378","346789","1379","37","89","37","","","2378","23578","23578","","","58","","3478","3478","134789","3578","345789","13579","37","","","","","78","","78","","","","","345","34","39","","359","","","","37","3578","378","2378","23578","","","",""]}';

describe('HiddenQuadReducer', () => {
    const sut = new HiddenQuadReducer();

    it('reduces a column quad', () => {
        const board = BoardSerializer.Deserialize(columnQuadPuzzle);

        sut.reduceOptions(board);

        const a = getOptions(board, 7, 1);
        expect(a).not.toContain(4);
    });

    it('reduces a square quad', () => {
        const board = BoardSerializer.Deserialize(squareQuadPuzzle);

        sut.reduceOptions(board);

        const a = getOptions(board, 3, 3);
        expect(a).not.toContain(3);
        expect(a).not.toContain(7);
        expect(a).not.toContain(8);

        const b = getOptions(board, 5, 3);
        expect(b).not.toContain(3);
        expect(b).not.toContain(7);
        expect(b).not.toContain(8);

        const c = getOptions(board, 3, 5);
        expect(c).not.toContain(3);
        expect(c).not.toContain(7);
        expect(c).not.toContain(8);

        const d = getOptions(board, 5, 5);
        expect(d).not.toContain(3);
        expect(d).not.toContain(5);
        expect(d).not.toContain(7);
        expect(d).not.toContain(8);
    });

    it('suggests a column quad', () => {
        const board = BoardSerializer.Deserialize(columnQuadPuzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.causes.map(c => c.space.location()).join(',')).toBe('D8,E8,F8,B8');
    });

    it('suggests a square quad', () => {
        const board = BoardSerializer.Deserialize(squareQuadPuzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.causes.map(c => c.space.location()).join(',')).toBe('D4,F4,D6,F6');
    });

});
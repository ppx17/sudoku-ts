import {emptyBoard, getOptions, setOptions} from "./helpers";
import {HiddenPairReducer} from "lib/algorithms/reducers/hiddenSetReducers";
import {Board} from 'lib/board';
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

describe('HiddenPairReducer', () => {
    const sut = new HiddenPairReducer();

    const hiddenPairBoard = (): Board => {
        const board = emptyBoard();

        // A 1,2 pair
        setOptions(board, 0, 0, [1, 2, 5, 7]);
        setOptions(board, 2, 2, [1, 2, 3, 9]);
        // Unrelated
        setOptions(board, 1, 1, [4, 8]);

        return board;
    };

    it('removes all options except the hidden pair in the square', () => {

        const board = hiddenPairBoard();

        expect(sut.reduceOptions(board)).toBe(true);

        expect(getOptions(board, 0, 0,)).not.toContain(5);
        expect(getOptions(board, 0, 0,)).not.toContain(7);
        expect(getOptions(board, 0, 0,)).toContain(1);
        expect(getOptions(board, 0, 0,)).toContain(2);

        expect(getOptions(board, 2, 2,)).not.toContain(3);
        expect(getOptions(board, 2, 2,)).not.toContain(9);
        expect(getOptions(board, 2, 2,)).toContain(1);
        expect(getOptions(board, 2, 2,)).toContain(2);

        expect(getOptions(board, 1, 1,)).toContain(4);
        expect(getOptions(board, 1, 1,)).toContain(8);
    });

    it('suggest a hidden pair', () => {
        const board = hiddenPairBoard();

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);

        expect(suggestion.effects).toHaveLength(2);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([5, 7]);
        expect(suggestion.effects[1].removeOptions).toStrictEqual([3, 9]);
    })
});
import {ExposedPairReducer} from "lib/algorithms/reducers/exposedPairReducer";
import {emptyBoard, getOptions, setOptions} from "./helpers";

describe('ExposedPairReducer', () => {
    const sut = new ExposedPairReducer();

    it('removes a pair from the same square', () => {
        const board = emptyBoard();

        // A 1,2 pair
        setOptions(board, 0, 0, [1, 2]);
        setOptions(board, 0, 1, [1, 2]);

        // In the same square
        setOptions(board, 2, 2, [1, 2, 3]);

        sut.reduceOptions(board);

        expect(getOptions(board, 2, 2,)).not.toContain(1);
        expect(getOptions(board, 2, 2,)).not.toContain(2);
        expect(getOptions(board, 2, 2,)).toContain(3);
    });

    it('removes a pair from the same row', () => {
        const board = emptyBoard();

        // A 1,2 pair
        setOptions(board, 0, 0, [1, 2]);
        setOptions(board, 0, 1, [1, 2]);

        // In the same row
        setOptions(board, 0, 6, [1, 2, 3]);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 6,)).not.toContain(1);
        expect(getOptions(board, 0, 6,)).not.toContain(2);
        expect(getOptions(board, 0, 6,)).toContain(3);
    });

    it('removes a pair from the same column', () => {
        const board = emptyBoard();

        // A 1,2 pair
        setOptions(board, 0, 0, [1, 2]);
        setOptions(board, 1, 0, [1, 2]);

        // In the same column
        setOptions(board, 6, 0, [1, 2, 3]);

        sut.reduceOptions(board);

        expect(getOptions(board, 6, 0,)).not.toContain(1);
        expect(getOptions(board, 6, 0,)).not.toContain(2);
        expect(getOptions(board, 6, 0,)).toContain(3);
    });

    it('it removes nothing when the pair has more options', () => {
        const board = emptyBoard();

        // A broken 1,2 pair
        setOptions(board, 0, 0, [1, 2]);
        setOptions(board, 1, 0, [1, 2, 4]);

        // In the same column
        setOptions(board, 6, 0, [1, 2, 3]);

        sut.reduceOptions(board);

        expect(getOptions(board, 6, 0,)).toContain(1);
        expect(getOptions(board, 6, 0,)).toContain(2);
        expect(getOptions(board, 6, 0,)).toContain(3);
    });

    it('suggests a pair from the same square', () => {
        const board = emptyBoard();

        // A 1,2 pair
        setOptions(board, 0, 0, [1, 2]);
        setOptions(board, 0, 1, [1, 2]);

        // In the same square
        setOptions(board, 2, 2, [1, 2, 3]);

        const suggestion = sut.suggest(board);

        expect(suggestion).not.toBeNull();
        expect(suggestion.causes).toHaveLength(2);
        expect(suggestion.causes[0].space).toBe(board.board[0][0]);
        expect(suggestion.causes[0].highlightBlue).toHaveLength(1);
        expect(suggestion.causes[0].highlightBlue).toContain(1);
        expect(suggestion.causes[0].highlightGreen).toHaveLength(1);
        expect(suggestion.causes[0].highlightGreen).toContain(2);

        expect(suggestion.causes[1].space).toBe(board.board[1][0]);
        expect(suggestion.causes[1].highlightBlue).toHaveLength(1);
        expect(suggestion.causes[1].highlightBlue).toContain(1);
        expect(suggestion.causes[1].highlightGreen).toHaveLength(1);
        expect(suggestion.causes[1].highlightGreen).toContain(2);

        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].space).toBe(board.board[2][2]);
        expect(suggestion.effects[0].removeOptions).toHaveLength(2);
        expect(suggestion.effects[0].removeOptions).toContain(1);
        expect(suggestion.effects[0].removeOptions).toContain(2);
    });
});
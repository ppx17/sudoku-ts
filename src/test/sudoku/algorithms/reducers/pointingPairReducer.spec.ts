import {PointingPairReducer} from "lib/algorithms/reducers/pointingPairReducer";
import {emptyBoard, getOptions, setOptions} from "./helpers";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";

describe('PointingPairReducer', () => {
    const sut = new PointingPairReducer();

    it('reduces pair in same row', () => {
        const board = emptyBoard();

        setOptions(board, 0, 0, [1, 2, 3]);
        setOptions(board, 7, 0, [1, 8, 6]); // only two options where the 1 can go in this square
        setOptions(board, 8, 0, [1, 4, 5]);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 0)).not.toContain(1);
        expect(getOptions(board, 0, 0)).toContain(2);
        expect(getOptions(board, 0, 0)).toContain(3);
    });

    it('does not reduces pair in another row', () => {
        const board = emptyBoard();

        setOptions(board, 0, 0, [1, 2, 3]);
        setOptions(board, 7, 1, [1, 8, 6]);
        setOptions(board, 8, 1, [1, 4, 5]);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 0)).toContain(1);
        expect(getOptions(board, 0, 0)).toContain(2);
        expect(getOptions(board, 0, 0)).toContain(3);
    });

    it('reduces pair in same column', () => {
        const board = emptyBoard();

        setOptions(board, 0, 0, [1, 2, 3]);
        setOptions(board, 0, 7, [1, 8, 6]);
        setOptions(board, 0, 8, [1, 4, 5]);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 0)).not.toContain(1);
        expect(getOptions(board, 0, 0)).toContain(2);
        expect(getOptions(board, 0, 0)).toContain(3);
    });

    it('does not reduces pair in another column', () => {
        const board = emptyBoard();

        setOptions(board, 0, 0, [1, 2, 3]);
        setOptions(board, 1, 7, [1, 8, 6]);
        setOptions(board, 1, 8, [1, 4, 5]);

        sut.reduceOptions(board);

        expect(getOptions(board, 0, 0)).toContain(1);
        expect(getOptions(board, 0, 0)).toContain(2);
        expect(getOptions(board, 0, 0)).toContain(3);
    });

    it('suggests pair in same column', () => {
        const board = emptyBoard();

        setOptions(board, 0, 0, [1, 2, 3]);
        setOptions(board, 0, 7, [1, 8, 6]);
        setOptions(board, 0, 8, [1, 4, 5]);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);

        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.effects[0].space).toBe(board.board[0][0]);
        expect(suggestion.effects[0].removeOptions).toStrictEqual([1]);

        expect(suggestion.causes).toHaveLength(2);
        expect(suggestion.causes[0].space).toBe(board.board[7][0]);
        expect(suggestion.causes[1].space).toBe(board.board[8][0]);
        expect(suggestion.causes[0].highlightBlue).toStrictEqual([1]);
        expect(suggestion.causes[1].highlightBlue).toStrictEqual([1]);
    });
});
import {Board} from "lib/board";

export function getOptions(board: Board, x: number, y: number): number[] {
    return board.board[y][x].options.toNumberList();
}

export function setOptions(board: Board, x: number, y: number, options: number[]) {
    return board.board[y][x].options.fromNumberList(options);
}

export function emptyBoard(): Board {
    const board = new Board();
    board.generateSpaces(false);
    return board;
}
import {BoardSerializer} from "lib/board";
import {ReduceSuggestion} from "lib/algorithms/reducers/reducer";
import {XyChainsReducer} from "lib/algorithms/reducers/xyChainsReducer";

const puzzle = '{"values":"080103070090506000001408020578241639143659782926837451037905200000304097419782060","notes":["26","","245","","29","","59","","456","37","","24","","27","","18","14","348","37","56","","","79","","359","","356","","","","","","","","","","","","","","","","","","","","","","","","","","","","68","","","","16","","","14","48","268","56","25","","16","","18","","","","","","","","","35","","35"]}';
const invalidChain = '{"values":"649182735802030960703090800195274683287361450436958200504810390301049508978523146","fixed":"000100101101000010000000000000001001000011100011010000100100110000010000111010001","notes":["36","49","39","","8","2","","3","","","15","","47","379","57","9","","14","1367","15","3","46","79","56","289","12","124","1","59","5","279","7","","125689","178","","2","8","7","2379","","","","578","9","1247","","","9","","8","29","17","17","","26","4","","17","67","","","27","3","26","1","67","","679","258","27","278","","","","5","","35","1","4",""]}';

describe('XyChainsReducer', () => {
    const sut = new XyChainsReducer();

    it('suggests a xy-chain', () => {
        const board = BoardSerializer.Deserialize(puzzle);

        const suggestion = sut.suggest(board);

        expect(suggestion).toBeInstanceOf(ReduceSuggestion);
        expect(suggestion.effects).toHaveLength(1);
        expect(suggestion.causes).toHaveLength(6);
    });

    it('should not find a chain where the end piece is linked by the number it tries to use', () => {
        const board = BoardSerializer.Deserialize(invalidChain);

        const suggestion = sut.suggest(board);

        const causeSpaces = suggestion.causes.map(c => c.space);

        // This test catches a bug where the chain will not check the weak link to the target cell,
        // then there is a chain following C8, H8, H4, G6, B6, B2, B9 on the 1. However the chain forces a 4 in
        // B9 and not the 1, so it is an invalid exclusion.

        expect(suggestion.effects[0].removeOptions).not.toBe([1]);
        expect(causeSpaces).not.toContain(board.board[7][3]); // H4
        expect(causeSpaces).not.toContain(board.board[7][7]); // H8
        expect(causeSpaces).not.toContain(board.board[1][5]); // B6

        // There is however a valid chain on the 2 following C8, C2, C6, G6 and G9
        expect(suggestion.effects[0].removeOptions).toStrictEqual([2]);

    })
});




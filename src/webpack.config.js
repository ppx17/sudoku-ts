const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require("copy-webpack-plugin");

const web = {
    entry: [
        './src/web/index.ts',
        './src/web/style.scss',
    ],
    devtool: process.env.NODE_ENV === 'production' ? undefined : 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            'lib': path.resolve(__dirname, 'src/lib'),
        }
    },
    output: {
        filename: 'bundle.[contenthash].js',
        path: path.resolve(__dirname, 'public'),
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            'title': 'Sudoku-TS',
            'scriptLoading': 'defer',
            'template': 'src/web/index.html',
        }),
        new MiniCssExtractPlugin({
            'filename': 'style.[contenthash].css',
        }),
        new CopyPlugin({
            patterns: [
                {from: path.resolve(__dirname, 'assets'), to: path.resolve(__dirname, 'public/assets')},
                {from: path.resolve(__dirname, 'puzzles'), to: path.resolve(__dirname, 'public/puzzles')},
            ]
        })
    ]
};
const worker = {
    entry: [
        './src/web/worker.ts',
    ],
    devtool: process.env.NODE_ENV === 'production' ? undefined : 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts'],
        alias: {
            'lib': path.resolve(__dirname, 'src/lib'),
        }
    },
    output: {
        filename: 'worker.js',
        path: path.resolve(__dirname, 'public'),
    },
    plugins: []
};

const cli = {
    target: 'async-node15.5',
    mode: "production",
    devtool: undefined,
    entry: [
        './src/cli/cli.ts',
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            'lib': path.resolve(__dirname, 'src/lib'),
        }
    },
    output: {
        filename: 'sudoku.js',
        path: path.resolve(__dirname, 'cli'),
    },
};

module.exports = [web, worker, cli];